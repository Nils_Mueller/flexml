import logging
import sys
import os
import time
import yaml
import argparse
from core.Pipeline import Pipeline

def setup_logger(show_logs=True, store_logs=True):
    """
    Sets up the logging configuration based on the provided options for displaying
    and storing logs.

    Parameters:
        show_logs (bool or str): Whether to show logs in the console. Defaults to True.
        store_logs (bool or str): Whether to store logs in a file. Defaults to True.

    Returns:
        str: The timestamp used for the log filename.

    The function configures logging to display log messages on the console and optionally
    store them in a log file. The log file is named based on the current timestamp.
    If `show_logs` or `store_logs` is set to "True", the corresponding behavior will be enabled.
    """
    show_logs = eval(show_logs) if isinstance(show_logs, str) else show_logs
    store_logs = eval(store_logs) if isinstance(store_logs, str) else store_logs
    logtime=f"{time.time():.00f}"
    logname=f'log_{logtime}.log'
    logfile=os.path.join('logs', logname)
    handlers = [logging.FileHandler(logfile),logging.StreamHandler(sys.stdout)] if store_logs else [logging.StreamHandler(sys.stdout)]
    if store_logs or show_logs:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s.%(msecs)03d [%(threadName)-12.12s] %(levelname)-5.5s:%(name)s:%(message)s',
                            handlers=handlers)
    return logtime

def load_config(config_file):
    """
    Loads a configuration from the specified YAML file.

    Parameters:
        config_file (str): The path to the configuration YAML file.

    Returns:
        dict: The parsed configuration data as a dictionary.

    This function reads the specified YAML configuration file, parses it, and returns the
    data as a Python dictionary. It logs the action of reading the configuration file.
    """
    logging.info(f"Reading config file {config_file}")
    with open(config_file, 'r') as file:
        loaded_config = yaml.safe_load(file)
    return loaded_config

if __name__ == '__main__':
    argParser = argparse.ArgumentParser()
    argParser.add_argument("-c", "--config", type=str, required=True, help="Configuration file")
    argParser.add_argument("-shl", "--show_logs", type=str, default="True", help="Show logs")
    argParser.add_argument("-stl", "--store_logs", type=str, default="True", help="Store logs")
    args = argParser.parse_args()
    logtime = setup_logger(args.show_logs, args.store_logs)
    config = load_config(args.config)
    logging.info(f"Initializing FlexML with config file {args.config}...")
    Pipeline = Pipeline(config, logtime)
    Pipeline.run()
