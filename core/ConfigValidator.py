from pydantic import BaseModel
from datetime import datetime
import os

class BaseConfig(BaseModel):
    """
    A configuration class that check and stores the parameters and settings
    provided by the user via a yaml config file.

    Attributes:
        Name (str): The name of the configuration.
        Comment (str): An optional comment or description about the configuration.
        Pipeline (dict): Dictionary containing pipeline-specific settings.
        Datasets (dict): Dictionary containing dataset-related configurations.
        Preprocessing (dict): Dictionary for preprocessing configurations.
        Model (dict): Dictionary with model-specific settings.
        Model_selection (dict): Dictionary related to model selection parameters.
        Prediction (dict): Configuration for making predictions.
        Anomaly_detection (dict): Dictionary for anomaly detection settings.
        Results (dict): Settings related to the storage and presentation of results.
        Plotting (dict): Plotting-related configurations.
        Computation (dict): Dictionary containing computation-related configurations.

    Config:
        extra (str): Restricts extra fields from being included in the model configuration.
    """
    Name: str
    Comment: str
    Pipeline: dict
    Datasets: dict
    Preprocessing: dict
    Model: dict
    Model_selection: dict
    Prediction: dict
    Anomaly_detection: dict
    Results: dict
    Plotting: dict
    Computation: dict

    class Config:
        extra = "forbid"

def validate_hist_data_filename(v):
    """
    Validates the historical data file's filename.

    Ensures the file has a `.csv` extension and exists within the
    specified directory (`datasets/raw`).

    Args:
        v (str): The filename to validate.

    Returns:
        str: The validated filename.

    Raises:
        ValueError: If the filename doesn't have a `.csv` extension or doesn't exist.
    """
    if not v.endswith('.csv'):
        raise ValueError(f'{v} must be a CSV file')
    if not os.path.exists(os.path.join("datasets", "raw", v)):
        raise ValueError(f'{v} does not exist')
    return v

def validate_new_data_filename(v):
    """
    Validates the new data file's filename.

    Similar to `validate_hist_data_filename` but allows `None` values.

    Args:
        v (str or None): The filename to validate (or `None` if no file is provided).

    Returns:
        str or None: The validated filename or `None` if no file is provided.

    Raises:
        ValueError: If the filename doesn't have a `.csv` extension or doesn't exist.
    """
    if v is None:
        return v
    if not v.endswith('.csv'):
        raise ValueError(f'{v} must be a CSV file')
    if not os.path.exists(os.path.join("datasets", "raw", v)):
        raise ValueError(f'{v} does not exist')
    return v

def validate_scaling_method(v: str) -> str:
    """
    Validates the scaling method used for the model.

    Checks if the scaling method is either a string with values `minmax`, `standard`,
    or `no_scaling`, or if it's a dictionary containing required keys for defining
    a search space.

    Args:
        v (str or dict): The scaling method value or dictionary specifying search space.

    Returns:
        str: The validated scaling method or scaling method search space.

    Raises:
        ValueError: If the scaling method is invalid.
    """
    if isinstance(v, str):
        if v not in ["minmax", "standard", "no_scaling"]:
            err_msg = f"Invalid scaling_method value: {v}. Must be one of 'minmax', 'standard', or 'no_scaling'."
            raise ValueError(err_msg)
    elif isinstance(v, dict):
        if "choices" not in v or "type" not in v:
            err_msg = "Search space dict of the scaling method must contain 'choices' and 'type' keys."
            raise ValueError(err_msg)
        if v["type"] != "categorical":
            raise ValueError("The 'type' key in the dictionary must be 'categorical'.")
        if not isinstance(v["choices"], list):
            err_msg = "'choices' key must be a list."
            raise ValueError(err_msg)
        if not all(choice in ["minmax", "standard", "no_scaling"] for choice in v["choices"]):
            err_msg = "All values in 'choices' must be one of 'minmax', 'standard', or 'no_scaling'."
            raise ValueError(err_msg)
    else:
        raise ValueError("scaling_method must be either a string or a dictionary.")
    return v

def check_model_type_availability(v: str) -> str:
    """
    Checks if the specified model type is available in the `modelhandlers` directory.

    Searches the `modelhandlers` directory for the availability of the model type
    and raises an error if it's not found.

    Args:
        v (str): The model type to validate.

    Returns:
        str: The validated model type.

    Raises:
        ValueError: If the model type is not available in the directory.
    """
    config_validator_path = os.path.dirname(os.path.abspath(__file__))
    modelhandlers_path = os.path.join(config_validator_path, "..", "modelhandlers")
    implemented_modelhandlers = os.listdir(modelhandlers_path)
    available_models = [filename.replace('Handler.py', '') for filename in implemented_modelhandlers]
    if v not in available_models:
        err_msg = f"Model type {v} not implemented. Available models: {', '.join(available_models)}"
        raise ValueError(err_msg)
    return v

def check_num_samples_if_probabilistic(self):
    """
    Checks if the number of samples is greater than 1 when probabilistic predictions are selected.

    Ensures that if `probabilistic` is set to `True`, the `num_samples` must be greater than 1.

    Returns:
        self: The current configuration object instance.

    Raises:
        ValueError: If `num_samples` is not greater than 1 when `probabilistic` is `True`.
    """
    if self.probabilistic and not self.num_samples > 1:
        err_msg = "If probabilistic predictions are selected, num_samples must be greater 1."
        raise ValueError(err_msg)
    return self

def check_if_model_supports_probabilistic_predictions(self):
    """
    Validates if the selected model supports probabilistic predictions.

    If the model type does not support probabilistic predictions, raises an error.

    Returns:
        self: The current object instance.

    Raises:
        ValueError: If the model type does not support probabilistic predictions.
    """
    non_probabilistic_models = ["NaiveSeasonal"]
    if (self.type in non_probabilistic_models) and self.probabilistic:
        err_msg = f"{self.type} does not support probabilistic predictions."
        raise ValueError(err_msg)
    return self

def check_if_none_false_or_pos_int(v):
    """
    Validates if the value is either `None`, `False`, or a positive integer.

    This function is used to ensure that a parameter is either not set (`None`),
    disabled (`False`), or set to a positive integer.

    Args:
        v (Any): The value to validate.

    Returns:
        The input value `v` if valid.

    Raises:
        ValueError: If the value is neither `None`, `False`, nor a positive integer.
    """
    if v is None:
        return v  # Allow None
    if isinstance(v, bool):
        if v is True:
            raise ValueError("Value cannot be True, only False, a positive integer or None.")
    elif isinstance(v, int):
        if v < 1:
            raise ValueError("Value must be a positive integer.")
    else:
        raise ValueError("Value must be either a positive integer or False.")
    return v

def check_if_none_false_or_between_0_1(v):
    """
    Validates if the value is either `None`, `False`, or a float between 0 and 1.

    This function ensures that a parameter value is either not set (`None`),
    disabled (`False`), or a float in the range [0, 1].

    Args:
        v (Any): The value to validate.

    Returns:
        The input value `v` if valid.

    Raises:
        ValueError: If the value is neither `None`, `False`, nor a float between 0 and 1.
    """
    if v is None:
        return v  # Allow None
    if isinstance(v, bool):
        if v is True:
            raise ValueError("Value cannot be True, only False, between 0 and 1 or None.")
    elif isinstance(v, float):
        if 0 > v > 1:
            raise ValueError("Value must be a between 0 and 1.")
    else:
        raise ValueError("Value must be either between 0 and 1, None or False.")
    return v

def check_if_tscv_if_early_stopping_or_pruning(self):
    """
    Ensures that Time Series Cross-Validation (TSCV) is not used together
    with early stopping or pruning.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If Time Series Cross-Validation (CV_splits > 1) is used with early stopping or pruning.
    """
    if self.CV_splits > 1 and (self.pruning or self.early_stopping):
        err_msg = ("No time series cross-validation (CV_splits > 1) possible "
                   "when applying pruning or early stopping during model selection.")
        raise ValueError(err_msg)
    return self

def validate_anomaly_ground_truth(v):
    """
    Validates the format and contents of the anomaly ground truth data.

    Ensures that each item in the list is a dictionary with the required
    keys: `Type`, `Victim`, `Start`, `End`, and `Reasoning`. The `Start`
    and `End` fields should be in the correct datetime format and `Start`
    must be before `End`.

    Args:
        v (list or None): The anomaly ground truth data to validate, or `None` if not provided.

    Returns:
        list or None: The validated anomaly ground truth data, or `None` if not provided.

    Raises:
        ValueError: If the anomaly ground truth data is not in the expected format.
    """
    if v is None:
        return v

    # Check if input is a list
    if not isinstance(v, list):
        err_msg = "Anomaly_ground_truth must be a list of dictionaries."
        raise ValueError(err_msg)

    for idx, item in enumerate(v):
        # Check if each item is a dictionary
        if not isinstance(item, dict):
            err_msg = f"Item {idx + 1} in Anomaly_ground_truth must be a dictionary."
            raise ValueError(err_msg)

        # Check if the dictionary has the required keys
        required_keys = {"Type", "Victim", "Start", "End", "Reasoning"}
        missing_keys = required_keys - item.keys()
        if missing_keys:
            err_msg = f"Item {idx + 1} is missing keys: {', '.join(missing_keys)}."
            raise ValueError(err_msg)

        # Validate "Type" field (can be extended with a set of valid types)
        if not isinstance(item["Type"], str) or not item["Type"]:
            err_msg = f"Item {idx + 1} has an invalid Type. It must be a non-empty string."
            raise ValueError(err_msg)

        # Validate "Victim" field (non-empty string)
        if not isinstance(item["Victim"], str) or not item["Victim"]:
            err_msg = f"Item {idx + 1} has an invalid Victim. It must be a non-empty string."
            raise ValueError(err_msg)

        # Validate "Start" and "End" fields (valid datetime format)
        try:
            start_datetime = datetime.strptime(item["Start"], "%Y-%m-%d %H:%M:%S")
        except ValueError:
            err_msg = f"Item {idx + 1} has an invalid Start datetime format. Expected 'YYYY-MM-DD HH:MM:SS'."
            raise ValueError(err_msg)

        try:
            end_datetime = datetime.strptime(item["End"], "%Y-%m-%d %H:%M:%S")
        except ValueError:
            err_msg = f"Item {idx + 1} has an invalid End datetime format. Expected 'YYYY-MM-DD HH:MM:SS'."
            raise ValueError(err_msg)

        # Ensure Start is before End
        if start_datetime >= end_datetime:
            err_msg = f"Item {idx + 1}: Start datetime must be before End datetime."
            raise ValueError(err_msg)

        # Validate "Reasoning" field (non-empty string)
        if not isinstance(item["Reasoning"], str):
            err_msg = f"Item {idx + 1} has an invalid Reasoning. It must be a non-empty string."
            raise ValueError(err_msg)
    return v

def check_params_for_model_selection_status(self):
    """
    Validates the status of model selection and parameter space.

    Ensures that if model selection is enabled, there are parameters defined as value spaces.
    Similarly, if no parameters have value spaces defined, model selection should be disabled.

    Args:
        self: The current object instance.

    Returns:
        self: The current object instance.

    Raises:
        ValueError: If there is an inconsistency between model selection and defined value spaces for parameters.
    """
    model_hps = self.hyperparameters if self.hyperparameters else {}
    hp_with_scaling = model_hps | {"scaling_method": self.scaling_method}
    params_as_value_space = [param for param, val in hp_with_scaling.items() if isinstance(val, dict)]
    if not self.model_selection and params_as_value_space:
        err_msg = f"Parameter(s) {', '.join(params_as_value_space)} defined as value space, but model_selection=False."
        raise ValueError(err_msg)
    if self.model_selection and not params_as_value_space:
        err_msg = "No parameters defined as value space, but model_selection=True."
        raise ValueError(err_msg)
    return self

def check_for_shap_support(self):
    """
    Ensures that the selected model supports SHAP (Shapley Additive Explanations) values.

    If SHAP explainability plots are enabled, checks that the model type supports SHAP.

    Args:
        self: The current object instance.

    Returns:
        self: The current object instance.

    Raises:
        ValueError: If SHAP support is enabled for a model that does not support it.
    """
    no_shap_support_models = ["NaiveSeasonal", "StatsForecastAutoARIMA"]
    if self.save_shap_explainability_plots and (self.type in no_shap_support_models):
        err_msg = (f"{self.type} does not support Shap values. Use different model "
                   "type or set save_shap_explainability_plots=False.")
        raise ValueError(err_msg)
    return self

def check_steps_to_plot_feasibility(self):
    """
    Validates the feasibility of plotting requested forecasting steps.

    Ensures that the forecasted steps ahead and steps to plot align.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If the steps to plot are incompatible with the forecast horizon.
    """
    if self.steps_to_plot in [None, []]:
        return self
    if self.forecasting_horizon < max(self.steps_to_plot):
        err_msg = f"Steps to be plotted exceed forecasting horizon. Update forecasting_horizon or steps_to_plot."
        raise ValueError(err_msg)
    if not self.predict_full_horizon and any(self.forecasting_horizon != h for h in self.steps_to_plot):
        err_msg = f"Steps to be plotted include step that is not predicted, probably since predict_full_horizon=False."
        raise ValueError(err_msg)
    return self

def check_error_metric(self):
    """
    Validates if the selected error metric is compatible with the type of predictions.

    Ensures that the `PINBALL` error metric is only used for probabilistic models.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If the `PINBALL` error metric is used for a non-probabilistic model.
    """
    if not self.probabilistic and (self.error_metric == "PINBALL"):
        err_msg = "Error metric PINBALL only applicable to probabilistic predictions."
        raise ValueError(err_msg)
    return self

def check_probabilistic_for_anomaly_detection(self):
    """
    Ensures that anomaly detection is only used for probabilistic models.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If anomaly detection is enabled for a non-probabilistic model.
    """
    if not self.probabilistic and self.anomaly_detection:
        err_msg = "Anomaly detection currently only supported for probabilistic models."
        raise ValueError(err_msg)
    return self

def check_early_stopping_capability(self):
    """
    Checks if the selected model supports early stopping during model selection.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If early stopping is enabled for a model that does not support it.
    """
    early_stopping_models = ["RNNModel"]
    if (self.type not in early_stopping_models) and self.early_stopping:
        err_msg = f"{self.type} does not support early stopping during model selection."
        raise ValueError(err_msg)
    return self

def check_pruning_capability(self):
    """
    Checks if the selected model supports pruning during model selection.

    Args:
        self: The current config object instance.

    Returns:
        self: The current config object instance.

    Raises:
        ValueError: If pruning is enabled for a model that does not support it.
    """
    pruning_models = ["RNNModel", "LightGBMModel"]
    if (self.type not in pruning_models) and self.pruning:
        err_msg = f"{self.type} does not support trial pruning during model selection."
        raise ValueError(err_msg)
    return self
