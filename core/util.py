import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler, StandardScaler, FunctionTransformer


def flatten_dict(d):
    """
    Flattens a nested dictionary into a single-level dictionary.

    Args:
        d (dict): A dictionary that may contain nested dictionaries.

    Returns:
        dict: A flattened dictionary where nested keys are combined into single keys.
    """
    flattened = {}
    for k, v in d.items():
        if isinstance(v, dict):
            for sub_key, sub_value in v.items():
                flattened[sub_key] = sub_value
        else:
            flattened[k] = v
    return flattened

def extract_multistep_quantile_predictions(predictions, quantiles, forecasting_horizon, predict_full_horizon, columns):
    """
     Extracts a DataFrame containing multi-step quantile predictions for a given forecasting horizon.
     Extracts a df from the raw forecasts where each column corresponds to a specific step-ahead forecast.

     Args:
         predictions (list): A list of predictions.
         quantiles (list): A list of quantiles (e.g., [0.1, 0.5, 0.9]).
         forecasting_horizon (int): The forecasting horizon (e.g., the number of steps ahead).
         predict_full_horizon (bool): Whether the full horizon is predicted or just the final step.
         columns (list): The list of column names for the output DataFrame.

     Returns:
         pd.DataFrame: A DataFrame with multi-step quantile predictions.
     """
    predicted_horizons = range(1, forecasting_horizon + 1) if predict_full_horizon else [forecasting_horizon]

    # Initialize numpy array (x=steps ahead, y=num of quantiles, z=time steps) and timestamps list
    pred_array = np.empty(shape=(len(predicted_horizons), len(quantiles), len(predictions)))
    timestamps = []

    # Fill the pred_val_array with the predictions
    for w, series_at_t in enumerate(predictions):
        pred_array[:, :, w] = series_at_t.quantiles_df(quantiles).values.reshape(forecasting_horizon, len(quantiles))
        timestamps.append(series_at_t.time_index[len(predicted_horizons)-1])

    # Transform numpy array into dataframe
    prediction_df = pd.DataFrame(np.transpose(pred_array.reshape(len(predicted_horizons) * len(quantiles), pred_array.shape[2])), columns=columns, index=timestamps).rename_axis('Timestamps')

    # Shift time series depending on steps ahead (each row will contain all steps-ahead predictions for this specific t)
    for step in predicted_horizons:
        step_cols = [col for col in columns if col.startswith(f'{step}_')]
        prediction_df[step_cols] = prediction_df[step_cols].shift(-(forecasting_horizon - step))

    # Remove time steps at beginning and end where predictions of some steps-ahead predictions are missing
    prediction_df.dropna(inplace=True)

    return prediction_df

def extract_multistep_deterministic_predictions(predictions, forecasting_horizon, predict_full_horizon, columns):
    """
    Extracts a DataFrame containing multi-step deterministic predictions for a given forecasting horizon.
    Extracts a df from the raw forecasts where each column corresponds to a specific step-ahead forecast.

    Args:
        predictions (list): A list of forecast objects (e.g., from a model).
        forecasting_horizon (int): The forecasting horizon (e.g., the number of steps ahead).
        predict_full_horizon (bool): Whether to predict the full horizon or just the final step.
        columns (list): The list of column names for the output DataFrame.

    Returns:
        pd.DataFrame: A DataFrame with multi-step deterministic predictions.
    """

    predicted_horizons = range(1, forecasting_horizon + 1) if predict_full_horizon else [forecasting_horizon]

    # Initialize numpy array (x=steps ahead, y=quantiles=1, z=time steps) and timestamps list
    pred_array = np.empty(shape=(len(predicted_horizons), 1, len(predictions)))
    timestamps = []

    # Fill the pred_array with the predictions
    for w, series_at_t in enumerate(predictions):
        pred_array[:, :, w] = series_at_t.pd_dataframe().values
        timestamps.append(series_at_t.time_index[len(predicted_horizons) - 1])

    # Store all predictions for all steps ahead in a df
    prediction_df = pd.DataFrame(np.transpose(pred_array.reshape(len(predicted_horizons), pred_array.shape[2])),
                                 columns=columns, index=timestamps).rename_axis('Timestamps')

    # Shift time series depending on steps ahead
    for step in predicted_horizons:
        step_cols = [col for col in columns if col.startswith(f'{step}_')]
        prediction_df[step_cols] = prediction_df[step_cols].shift(-(forecasting_horizon - step))

    # Remove time steps at beginning and end where predictions of some steps-ahead predictions are missing
    prediction_df.dropna(inplace=True)

    return prediction_df

def column_names(forecasting_horizon, probabilistic, quantiles, predict_full_horizon):
    """
    Generates column names for the predictions based on the forecasting horizon, whether the predictions are
    probabilistic, and the quantiles.

    Args:
        forecasting_horizon (int): The number of steps ahead to forecast.
        probabilistic (bool): Whether the predictions are probabilistic (i.e., include quantiles).
        quantiles (list): A list of quantiles for probabilistic predictions.
        predict_full_horizon (bool): Whether to predict the full horizon or just the final step.

    Returns:
        list: A list of column names for the predictions.
    """
    columns = []

    predicted_horizons = range(1, forecasting_horizon + 1) if predict_full_horizon else [forecasting_horizon]

    for i in predicted_horizons:  # All steps ahead
        horizon = f"{i}_ahead"
        if probabilistic:
            for quantile_ in quantiles:
                columns.append(f"{horizon}_{quantile_}")  # All predicted quantiles
        else:
            columns.append(f"{horizon}")

    return columns

def scaler_initialization(scaling_method):
    """
    Initializes a scaler based on the specified scaling method.

    Args:
        scaling_method (str): The scaling method to use. Options are "minmax", "standard", or "no_scaling".

    Returns:
        sklearn.preprocessing: A scaler object corresponding to the specified method.

    Raises:
        ValueError: If an unsupported scaling method is provided.
    """
    if scaling_method == "minmax":
        scaler_selection = MinMaxScaler()
    elif scaling_method == "standard":
        scaler_selection = StandardScaler()
    elif scaling_method == "no_scaling":
        scaler_selection = FunctionTransformer(lambda x: x)
    else:
        raise ValueError(f"{scaling_method} not implemented yet!")
    return scaler_selection

def Prediction_format_changer(predictions, forecasting_horizon=False):
    """
    Changes the format of the prediction DataFrame by shifting the predictions for different steps ahead.

    Args:
        predictions (pd.DataFrame): A DataFrame containing the raw predictions.
        forecasting_horizon (int or False): The forecasting horizon. If False, the horizon is inferred from the DataFrame.

    Returns:
        pd.DataFrame: A DataFrame with shifted time series based on the forecast horizon.
    """
    Predictions = predictions

    if not forecasting_horizon:
        forecasting_horizon = int(Predictions.columns[-2].split("_ahead", 1)[0])
    else:
        forecasting_horizon = forecasting_horizon

    columns = Predictions.columns

    # Shift time series depending on steps ahead (each row will contain all steps-ahead predictions predicted at this t for the next steps ahead)
    for step in range(2, forecasting_horizon + 1):
        step_cols = [col for col in columns if col.startswith(f'{step}_')]
        Predictions[step_cols] = Predictions[step_cols].shift(-step+1)

    # Remove time steps at beginning and end where predictions of some steps-ahead predictions are missing
    Predictions.dropna(inplace=True)

    return Predictions


def Transform_weather_forecast_input_to_past_covariates(predictions, forecasting_horizon=False):

    """
    Transforms weather forecast predictions into a format suitable for past covariates, where each
    x-steps ahead forecast is represented as a separate input column for use in time series models.

    Instead of having one weather forecast feature which would have to be dynamically updated
    (due to better forecasts the closer we get), we transform each x-steps ahead forecast sequence into
    an input column. This input column can be fed to darts model as past covariate.

    weather forecast input row...
    [1 step ahead, 2 steps ahead, 3 steps ahead, ...]

    to...

    past covariate column
    [1 step ahead]
    [2 step ahead]
    [3 steps ahead]
    [...]

    Args:
        predictions (pd.DataFrame): A DataFrame containing weather forecast predictions.
        forecasting_horizon (int or False): The forecasting horizon. If False, the horizon is inferred from the DataFrame.

    Returns:
        pd.DataFrame: A DataFrame with transformed weather forecast inputs as past covariates.
    """
    Predictions = predictions
   # print(predictions.head())
    if not forecasting_horizon:
        forecasting_horizon = int(Predictions.columns[-2].split("_ahead", 1)[0])
    else:
        forecasting_horizon = forecasting_horizon

    columns = Predictions.columns

    # Shift time series depending on steps ahead (each row will contain all steps-ahead predictions predicted at this t for the next steps ahead)
    for step in range(1, forecasting_horizon + 1):
        step_cols = [col for col in columns if "ahead" in col]
        step_cols = [col for col in step_cols if col.split("_", 1)[1:2][0].startswith(f'{step}_')]
        Predictions[step_cols] = Predictions[step_cols].shift(-step)

    # Remove time steps at beginning and end where predictions of some steps-ahead predictions are missing
    Predictions.dropna(inplace=True)
    #print(predictions.head())
    return Predictions
