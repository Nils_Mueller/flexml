import numpy as np
from pydantic import BaseModel
from sklearn.metrics import mean_pinball_loss as mpl, mean_squared_error as mse, mean_absolute_error as mae
from configvalidators.ModelEvaluatorConfig import ModelEvaluatorConfig


class ModelEvaluator:
    """
    ModelEvaluator class to compute various evaluation metrics for model predictions.

    This class calculates performance metrics, including Pinball loss, RMSE, and MAE,
    for multi-step predictions. It also allows for custom evaluation metrics to be defined.
    The class uses configuration settings to adjust its behavior, such as forecasting horizons
    and quantiles.

    Attributes:
        me_cfg (ModelEvaluatorConfig): Configuration object with settings for model evaluation.
    """
    def __init__(self, config):
        """
        Initializes the ModelEvaluator object with the provided configuration.

        Args:
            config (BaseModel or dict): Configuration object or dict with settings for model evaluation.

        Raises:
            TypeError: If the config is not of type BaseModel or dict.
        """
        if isinstance(config, BaseModel):
            self.me_cfg = ModelEvaluatorConfig.from_config(config)
        elif isinstance(config, dict):
            self.me_cfg = ModelEvaluatorConfig.from_dict(config)
        else:
            err_msg = "Invalid config type in ModelEvaluator. Expected config object or dict."
            raise TypeError(err_msg)

    def custom_multistep(self, y_pred, y_true):
        """
        Custom multi-step evaluation function that must be implemented
        if the error metric is set to 'CUSTOM'.

        Args:
            y_pred (pd.DataFrame): DataFrame of predicted values.
            y_true (pd.Series): Series of true values.

        Raises:
            NotImplementedError: If this method is not implemented when
            'error_metric' is set to 'CUSTOM'.
        """
        err_msg = ("'def custom_multistep(self, y_pred, y_true):' function in core.ModelEvaluator must be "
                   "implemented if error_metric='CUSTOM'.")
        raise NotImplementedError(err_msg)

    def pinball_multistep(self, y_pred, y_true):
        """
        Calculates the Pinball loss for multi-step predictions.

        Pinball loss (quantile loss) is calculated for each quantile
        in the configuration over the prediction horizon.

        Args:
            y_pred (pd.DataFrame): DataFrame of predicted values (quantiles).
            y_true (pd.Series): Series of true values.

        Returns:
            np.ndarray: Pinball loss for each step ahead.
        """
        # Guarantee y_val is a series and not one-column df
        y_true = y_true.squeeze()

        if not self.me_cfg.predict_full_horizon:
            predicted_horizons = [self.me_cfg.forecasting_horizon]
        else:
            predicted_horizons = range(1, self.me_cfg.forecasting_horizon + 1)

        # Calculate pinball score for all quantile/step-ahead combinations
        errors = []

        for alpha in self.me_cfg.quantiles:
            error = mpl(y_true=np.transpose([y_true for _ in predicted_horizons]),
                        y_pred=y_pred.filter(regex=f"{alpha}"), alpha=alpha, multioutput="raw_values")
            errors.append(list(error))

        # Average over the quantiles for each step ahead individually->pinball score (quantile loss) for each step ahead
        return 2 * np.mean(np.array(errors), axis=0)  # Times 2 to have same unit as predicted value

    def rmse_multistep(self, y_pred, y_true):
        """
        Calculates the Root Mean Squared Error (RMSE) for multi-step predictions.

        RMSE is calculated for each step ahead in the prediction horizon.

        Args:
            y_pred (pd.DataFrame): DataFrame of predicted values.
            y_true (pd.Series): Series of true values.

        Returns:
            np.ndarray: RMSE for each step ahead.
        """
        # Guarantee y_val is a series and not one-column df
        y_true = y_true.squeeze()

        if not self.me_cfg.predict_full_horizon:
            predicted_horizons = [self.me_cfg.forecasting_horizon]
        else:
            predicted_horizons = range(1, self.me_cfg.forecasting_horizon + 1)

        # Calculate RMSE for all steps ahead at once
        try:
            errors = np.sqrt(mse(y_true=np.transpose([y_true for _ in predicted_horizons]),
                                 y_pred=y_pred.filter(regex="0.5"), multioutput="raw_values"))
        except:
            try:
                errors = np.sqrt(mse(y_true=np.transpose([y_true for _ in predicted_horizons]),
                                     y_pred=y_pred.drop(columns=["Timestamps", "ground_truth"]),
                                     multioutput="raw_values"))
            except:
                errors = np.sqrt(mse(y_true=np.transpose([y_true for _ in predicted_horizons]),
                                     y_pred=y_pred, multioutput="raw_values"))
        return errors

    def mae_multistep(self, y_pred, y_true):
        """
        Calculates the Mean Absolute Error (MAE) for multi-step predictions.

        MAE is calculated for each step ahead in the prediction horizon.

        Args:
            y_pred (pd.DataFrame): DataFrame of predicted values.
            y_true (pd.Series): Series of true values.

        Returns:
            np.ndarray: MAE for each step ahead.
        """
        # Guarantee y_val is a series and not one-column df
        y_true = y_true.squeeze()

        if not self.me_cfg.predict_full_horizon:
            predicted_horizons = [self.me_cfg.forecasting_horizon]
        else:
            predicted_horizons = range(1, self.me_cfg.forecasting_horizon + 1)

        # Calculate RMSE for all steps ahead at once
        try:
            errors = mae(y_true=np.transpose([y_true for _ in predicted_horizons]),
                         y_pred=y_pred.filter(regex="0.5"), multioutput="raw_values")
        except:
            try:
                errors = mae(y_true=np.transpose([y_true for _ in predicted_horizons]),
                             y_pred=y_pred.drop(columns=["Timestamps", "ground_truth"]),
                             multioutput="raw_values")
            except:
                errors = mae(y_true=np.transpose([y_true for _ in predicted_horizons]),
                             y_pred=y_pred, multioutput="raw_values")
        return errors

    def calculate_error_for_each_step_ahead(self, y_pred, y_true):
        """
        Calculates the error for each step ahead in the prediction horizon
        based on the configured error metric.

        The method calculates different errors (Pinball loss, RMSE, MAE, or custom)
        depending on the configuration setting for 'error_metric'.

        Args:
            y_pred (pd.DataFrame): DataFrame of predicted values.
            y_true (pd.Series): Series of true values.

        Returns:
            np.ndarray: The calculated error for each step ahead.
        """
        if self.me_cfg.error_metric == "PINBALL":
            return self.pinball_multistep(y_pred, y_true)
        if self.me_cfg.error_metric == "RMSE":
            return self.rmse_multistep(y_pred, y_true)
        if self.me_cfg.error_metric == "MAE":
            return self.mae_multistep(y_pred, y_true)
        if self.me_cfg.error_metric == "CUSTOM":
            return getattr(self, "custom_multistep")(y_pred, y_true)

    def average_error_of_all_steps_ahead(self, errors):
        """
        Averages the errors across all steps ahead.

        The method calculates the weighted average of the errors across all
        steps ahead in the prediction horizon, using the weights specified
        in the configuration.

        Args:
            errors (np.ndarray): Array of errors for each step ahead.

        Returns:
            float: The weighted average error across all steps ahead.
        """
        return np.average(errors, weights=None if self.me_cfg.equal_weights_pred_steps else self.me_cfg.error_weights)
