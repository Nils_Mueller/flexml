import pandas as pd
import numpy as np
import os
from pydantic import BaseModel
from configvalidators.DataLoaderConfig import DataLoaderConfig

class DataLoader:
    """
    DataLoader class for loading raw datasets and extracting target features
    and covariates based on a given configuration.

    This class loads historical and new datasets, processes them by selecting
    the appropriate features, converting data types, and setting the timestamps
    as the index.

    Attributes:
        dl_cfg (DataLoaderConfig): Configuration object that contains the settings
        for loading and processing data.
    """
    def __init__(self, config):
        """
        Initializes the DataLoader object with the provided configuration.

        Args:
            config (BaseModel or dict): Configuration object or dictionary containing
            the settings for data loading.

        Raises:
            TypeError: If the config is not of type BaseModel or dict.
        """
        if isinstance(config, BaseModel):
            self.dl_cfg = DataLoaderConfig.from_config(config)
        elif isinstance(config, dict):
            self.dl_cfg = DataLoaderConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in DataLoader. Expected config object or dict.")

    def _load_data(self, path_dataset):
        """
        Loads the dataset from the given path, processes it by selecting features,
        and returns it as a pandas DataFrame.

        The method processes the dataset based on the configuration in `self.dl_cfg` by:
        - Selecting the relevant features (e.g., target feature, future/past covariates).
        - Limiting the dataset size if `try_out_dataset` is specified.
        - Converting numerical columns to `float32` for memory efficiency.
        - Converting the `Timestamps` column to `datetime64` and setting it as the index.

        Args:
            path_dataset (str): Path to the dataset file.

        Returns:
            pd.DataFrame: Processed dataset with the selected features and `Timestamps`
            set as the index.
        """
        # Define which columns to load
        features = ["Timestamps", self.dl_cfg.target_feature]
        if self.dl_cfg.future_covariates:
            features.extend(self.dl_cfg.future_covariates)
        if self.dl_cfg.past_covariates:
            features.extend(self.dl_cfg.past_covariates)

        # Load data
        if self.dl_cfg.try_out_dataset:
            total_rows = sum(1 for row in open(path_dataset))
            data = pd.read_csv(path_dataset, usecols=features, nrows=int(self.dl_cfg.try_out_dataset*total_rows))
        else:
            data = pd.read_csv(path_dataset, usecols=features)

        # Ensure use of faster float32 data type
        data[data.columns.drop("Timestamps")] = data[data.columns.drop("Timestamps")].astype(np.float32)

        ### Datetime index ###
        data["Timestamps"] = data["Timestamps"].astype("datetime64[ns]")
        data = data.set_index('Timestamps')

        return data

    def load_historical_dataset(self):
        """
        Loads the historical dataset based on the configuration in `self.dl_cfg`.

        Constructs the file path using the `hist_data_filename` setting in the configuration,
        then loads and processes the dataset using the `_load_data` method.

        Returns:
            pd.DataFrame: Processed historical dataset.
        """
        path_hist_dataset = os.path.join("datasets", "raw", self.dl_cfg.hist_data_filename)
        return self._load_data(path_hist_dataset)

    def load_new_dataset(self):
        """
        Loads the new dataset based on the configuration in `self.dl_cfg`.

        Constructs the file path using the `new_data_filename` setting in the configuration,
        then loads and processes the dataset using the `_load_data` method.

        Returns:
            pd.DataFrame: Processed new dataset.
        """
        path_new_dataset = os.path.join("datasets", "raw", self.dl_cfg.new_data_filename)
        return self._load_data(path_new_dataset)
