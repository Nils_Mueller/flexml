import logging
from importlib import import_module
from time import time
from core.ConfigValidator import BaseConfig
from core.DataLoader import DataLoader
from core.PreProcessor import PreProcessor
from core.ModelEvaluator import ModelEvaluator
from core.AnomalyDetector import AnomalyDetector
from core.ResultStorer import ResultStorer
from core.Plotter import Plotter
from core.ModelSelector import ModelSelector
from configvalidators.PipelineConfig import PipelineConfig

class Pipeline:
    """
    A class that orchestrates the end-to-end process of model selection, prediction,
    evaluation and interpretion.

    Attributes:
        pl_cfg (PipelineConfig): Configuration object containing the settings for the pipeline.
        logtime (float): Timestamp used for logging purposes.
        model_handler (object): The handler responsible for interacting with the model (e.g., fitting, prediction).
        data_loader (DataLoader): Responsible for loading the historical and new datasets.
        data_preprocessor (PreProcessor): Responsible for preprocessing and scaling the data.
        model_selector (ModelSelector): Responsible for selecting the optimal model and its parameters.
        model_evaluator (ModelEvaluator): Responsible for evaluating model performance using various metrics.
        anomaly_detector (AnomalyDetector): Responsible for detecting and flagging anomalies in the predictions.
        result_storer (ResultStorer): Responsible for saving results such as predictions and metrics.
        plotter (Plotter): Responsible for visualizing the results, including predictions and explainability plots.
    """
    def __init__(self, config_raw, logtime=time()):
        """
        Initializes the pipeline with the provided configuration and various pipeline components.

        Args:
            config_raw (dict): Raw configuration dictionary containing pipeline settings.
            logtime (float): The timestamp used for logging purposes (defaults to current time).
        """
        # Validate and save the config
        config = BaseConfig(**config_raw)
        self.pl_cfg = PipelineConfig.from_config(config)

        # Store logtime
        self.logtime = logtime

        # Initialize pipeline components
        m_handler_name = f"{self.pl_cfg.type}Handler"
        self.model_handler = getattr(import_module(f"modelhandlers.{m_handler_name}"), f"{m_handler_name}")(config, logtime)
        self.data_loader = DataLoader(config)
        self.data_preprocessor = PreProcessor(config)
        self.model_evaluator = ModelEvaluator(config)
        self.anomaly_detector = AnomalyDetector(config)
        self.result_storer = ResultStorer(config, logtime)
        self.plotter = Plotter(config, logtime)
        self.model_selector = ModelSelector(config, self.model_handler, self.data_preprocessor,
                                            self.model_evaluator, self.plotter, self.result_storer, logtime)

        # Configure a logger if not already configured
        if not logging.getLogger().hasHandlers():
            logging.basicConfig(level=logging.INFO,
            format='%(asctime)s.%(msecs)03d [%(threadName)-12.12s] %(levelname)-5.5s:%(name)s:%(message)s')

    def run(self):
        """
        Executes the pipeline, including loading historical data, preprocessing,
        model selection (if needed), training the model, making predictions on
        test and new data, and saving results such as metrics and plots.

        This method handles both the selection of models and the prediction of
        new or test data based on the configuration.
        """
        # Load historical dataset
        data_hist_raw = self.data_loader.load_historical_dataset()

        # Pre-process historical data
        X_hist, y_hist = self.data_preprocessor.preprocess_data(data_hist_raw)

        # Model selection or loading pre-defined parameters
        if self.pl_cfg.model_selection:
            model_params, scaling_method = self.model_selector.determine_optimal_params(X_hist, y_hist)
        else:
            model_params, scaling_method = self.model_handler.get_predefined_params(), self.pl_cfg.scaling_method

        # Initialize model
        model = self.model_handler.initialize_model(model_params)

        # Predict test data
        if self.pl_cfg.predict_test_data:
            self.fit_model_and_predict_test_data(model, scaling_method, X_hist, y_hist)

        # Predict new data
        if self.pl_cfg.predict_new_data:

            # Load new data
            data_new_raw = self.data_loader.load_new_dataset()

            # Pre-process new data
            X_new, y_new = self.data_preprocessor.preprocess_data(data_new_raw)

            self.refit_model_and_predict_new_data(model, scaling_method, X_new, y_new, X_hist, y_hist)

        logging.info(f"Run completed in {time() - float(self.logtime)} seconds.")

    def fit_model_and_predict_test_data(self, model, scaling_method, X_hist, y_hist):
        """
        Trains the model on the historical training data and evaluates its performance on
        the test set. Also fits anomaly detector if selected.

        Args:
            model (object): The model to be trained and used for predictions.
            scaling_method (str): The method used to scale the data.
            X_hist (pd.DataFrame): The covariates of the historical dataset.
            y_hist (pd.Series): The target feature of the historical dataset.
        """
        # Split into training and test data
        X_training, X_test, y_training, y_test = self.data_preprocessor.split_data_into_training_test(X_hist, y_hist)

        # Scale data
        X_training, X_test, y_training, y_test, y_scaler = self.data_preprocessor.scale_data(x_train=X_training,
                                                                                             x_test=X_test,
                                                                                             y_train=y_training,
                                                                                             y_test=y_test,
                                                                                             scaling_method=scaling_method)

        # Fit model to historical training data
        model = self.model_handler.fit_model_for_prediction(model, X_training, y_training)

        # Predict historical test data
        y_pred_test = self.model_handler.predict_test_set(model, X_training, X_test, y_training, y_test, y_scaler)

        # Fit anomaly detector to historical prediction test data
        if self.pl_cfg.anomaly_detection:
            self.anomaly_detector.fit(y_pred_test)
            y_pred_test = self.anomaly_detector.add_anomaly_score(y_pred_test)

        # Save test predictions
        if self.pl_cfg.save_test_set_predictions:
            self.result_storer.save_test_set_predictions(y_pred_test)

        # Save metrics for test predictions
        if self.pl_cfg.save_test_set_metrics:
            y_true = y_pred_test["ground_truth"]
            rmse = self.model_evaluator.rmse_multistep(y_pred_test, y_true)
            mae = self.model_evaluator.mae_multistep(y_pred_test, y_true)
            pinball = self.model_evaluator.pinball_multistep(y_pred_test, y_true) if self.pl_cfg.probabilistic else None
            custom = self.model_evaluator.custom_multistep(y_pred_test, y_true) if self.pl_cfg.error_metric == "CUSTOM" else None
            self.result_storer.save_test_set_metrics(rmse, mae, pinball, custom)

        # Save test prediction plots
        if self.pl_cfg.save_test_set_predictions_plots:
            self.plotter.plot_test_predictions(y_pred_test)

        # Save Shap explainer summary plots
        if self.pl_cfg.save_shap_explainability_plots:
            self.plotter.plot_shap_explainer(model)

    def refit_model_and_predict_new_data(self, model, scaling_method, X_new, y_new, X_hist, y_hist):
        """
         Re-trains the model using the entire historical dataset and makes predictions
         on new data. Also flags anomalies in the new data predictions if enabled.

         Args:
             model (object): The model to be retrained and used for predictions.
             scaling_method (str): The method used to scale the data.
             X_new (pd.DataFrame): Covariates of the new dataset.
             y_new (pd.Series): Target feature of the new dataset.
             X_hist (pd.DataFrame): Covariates of the historical dataset.
             y_hist (pd.Series): arget feature of the historical dataset.
         """
        # Scale new data
        X_hist, X_new, y_hist, y_new, y_scaler = self.data_preprocessor.scale_data(x_train=X_hist,
                                                                                   x_test=X_new,
                                                                                   y_train=y_hist,
                                                                                   y_test=y_new,
                                                                                   scaling_method=scaling_method)

        # Re-fit model to entire historical data
        model = self.model_handler.fit_model_for_prediction(model, X_hist, y_hist)

        # Predict new data
        y_pred_new = self.model_handler.predict_new_set(model, X_hist, X_new, y_hist, y_new, y_scaler)

        # Flag anomalies in new data
        if self.pl_cfg.anomaly_detection:
            y_pred_new = self.anomaly_detector.flag_anomalies(y_pred_new)

        # Save new predictions
        if self.pl_cfg.save_new_set_predictions:
            self.result_storer.save_new_set_predictions(y_pred_new)

        # Save metrics for new predictions
        if self.pl_cfg.save_new_set_metrics:
            y_true = y_pred_new["ground_truth"]
            rmse = self.model_evaluator.rmse_multistep(y_pred_new, y_true)
            mae = self.model_evaluator.mae_multistep(y_pred_new, y_true)
            pinball = self.model_evaluator.pinball_multistep(y_pred_new, y_true) if self.pl_cfg.probabilistic else None
            custom = self.model_evaluator.custom_multistep(y_pred_new, y_true) if self.pl_cfg.error_metric == "CUSTOM" else None
            self.result_storer.save_new_set_metrics(rmse, mae, pinball, custom)

        # Save new prediction plots
        if self.pl_cfg.save_new_set_predictions_plots:
            self.plotter.plot_new_predictions(y_pred_new)
