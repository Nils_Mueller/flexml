from pydantic import BaseModel
import logging
from configvalidators.AnomalyDetectorConfig import AnomalyDetectorConfig

class AnomalyDetector:
    """
    AnomalyDetector class is responsible for detecting anomalies in predictions
    made by a time-series forecasting model. The class uses probabilistic scoring,
    anomaly thresholds, and a flagging mechanism to identify and classify anomalies.

    Attributes:
        ad_cfg (AnomalyDetectorConfig): Configuration object that contains the settings
        for anomaly detection.
        highest_anomaly_score (float): Highest anomaly score during test set serving as
        reference for normalizing anomaly scores on new set.

    """
    def __init__(self, config):
        """
          Initializes the AnomalyDetector instance with a configuration object or dictionary.

          Args:
              config (Union[BaseModel, dict]): Configuration object or dictionary containing
              settings for the anomaly detector.

          Raises:
              TypeError: If the config is neither a Pydantic model nor a dictionary.
          """
        if isinstance(config, BaseModel):
            self.ad_cfg = AnomalyDetectorConfig.from_config(config)
        elif isinstance(config, dict):
            self.ad_cfg = AnomalyDetectorConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in AnomalyDetector. Expect config object or dict.")

        self.highest_anomaly_score = None


    def fit(self, y_pred):
        """
        Fits the anomaly detector on the given test set predictions.

        This method computes the anomaly scores using the probabilistic scorer
        and stores the highest anomaly score encountered during the test set evaluation.

        Args:
            y_pred (DataFrame): A pandas DataFrame containing predictions and ground truth.
        """
        logging.info(f"Fitting the anomaly detector to test set predictions...")
        # Add anomaly score to prediction df of test set
        y_pred = self.probabilistic_scorer(predictions=y_pred,
                                           upper_quantile=self.ad_cfg.quantiles[-1],
                                           lower_quantile=self.ad_cfg.quantiles[0],
                                           window=self.ad_cfg.anomaly_window)

        # Fit detector (max anomaly score during test set is 1)
        self.highest_anomaly_score = y_pred["Scores"].max()

    def add_anomaly_score(self, y_pred):
        """
        Adds the normalized anomaly score to the predictions DataFrame.

        The anomaly scores are normalized by dividing by the highest anomaly
        score observed during fitting on test set. If `fit()` has not been
        called yet, it raises an error.

        Args:
            y_pred (DataFrame): A pandas DataFrame containing anomaly scores.

        Returns:
            DataFrame: The input DataFrame with a 'Scores' column normalized with
            the highest anomaly score during test predictions.

        Raises:
            ValueError: If `fit()` has not been called before using this method.
        """
        if self.highest_anomaly_score is None:
            err_msg = "Anomaly Detector needs to be fitted before adding anomaly scores to predictions."
            raise ValueError(err_msg)
        y_pred["Scores"] = y_pred["Scores"] / self.highest_anomaly_score
        y_pred["Scores"] = y_pred["Scores"].fillna(0)
        return y_pred

    def flag_anomalies(self, y_pred):
        """
        Flags anomalies in predictions based on the computed anomaly score and threshold.

        Anomalies are identified when the anomaly score exceeds the specified threshold
        in the configuration. The method also classifies anomalies into different types
        (e.g., positive deviation, negative deviation).

        Args:
            y_pred (DataFrame): A pandas DataFrame containing predictions and ground truth.

        Returns:
            DataFrame: The input DataFrame with additional columns ('Flag' and 'Flag_direction')
            representing anomaly flags and anomaly classifications.
        """
        logging.info(f"Starting anomaly detection in new dataset...")
        # Add anomaly score to prediction df of test set
        y_pred = self.probabilistic_scorer(predictions=y_pred,
                                      upper_quantile=self.ad_cfg.quantiles[-1],
                                      lower_quantile=self.ad_cfg.quantiles[0],
                                      window=self.ad_cfg.anomaly_window)

        y_pred["Scores"] = y_pred["Scores"] / self.highest_anomaly_score
        y_pred["Scores"] = y_pred["Scores"].fillna(0)

        # Determine anomalies by comparison to the threshold
        y_pred['Flag'] = (y_pred["Scores"] > self.ad_cfg.threshold).astype(int)

        if self.ad_cfg.anomaly_types:
            y_pred = self._anomaly_classification(y_pred)

        return y_pred

    @staticmethod
    def _anomaly_classification(y_pred):
        """
        Classifies anomalies based on the deviation of predictions from the ground truth.

        The anomalies are classified as:
            - Positive deviation (1)
            - Negative deviation (-1)
            - Positive deviation with zero ground truth (2)
            - Negative deviation with zero ground truth (-2)

        Args:
            y_pred (DataFrame): A pandas DataFrame containing predictions and ground truth.

        Returns:
            DataFrame: The input DataFrame with an additional 'Flag_direction' column
            representing the type of anomaly.
        """
        flag_direction = len(y_pred) * [0]

        for idx in range(len(y_pred)):

            # positive deviation (1)
            if (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] < 0) & (
                    y_pred['ground_truth'].iloc[idx] != 0):
                flag_direction[idx] = 1

            # negative deviation (-1)
            elif (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] > 0) & (
                    y_pred['ground_truth'].iloc[idx] != 0):
                flag_direction[idx] = -1

            # positive deviation and zero (2)
            elif (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] < 0) & (
                    y_pred['ground_truth'].iloc[idx] == 0):
                flag_direction[idx] = 2

            # negative deviation and zero (-2)
            elif (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] > 0) & (
                    y_pred['ground_truth'].iloc[idx] == 0):
                flag_direction[idx] = -2


            elif (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] == 0):
                flag_direction[idx] = flag_direction[idx - 1]
            elif (y_pred['Flag'].iloc[idx] != 0) & (
                    y_pred['1_ahead_0.5'].iloc[idx] - y_pred['ground_truth'].iloc[idx] == 0) & (
                    y_pred['ground_truth'].iloc[idx] == 0):
                flag_direction[idx] = flag_direction[idx - 1]

        y_pred['Flag_direction'] = flag_direction

        return y_pred

    @staticmethod
    def probabilistic_scorer(predictions, upper_quantile, lower_quantile, window):
        """
         Computes the anomaly score for each prediction based on the probabilistic forecast.

         The score is determined by the distance between the ground truth and the forecasted
         values, with respect to the specified upper and lower quantiles. The scores are then
         smoothed using a rolling window average.

         Args:
             predictions (DataFrame): A pandas DataFrame containing predictions and ground truth.
             upper_quantile (float): The upper quantile used for the forecast interval.
             lower_quantile (float): The lower quantile used for the forecast interval.
             window (int): The window size for the rolling mean.

         Returns:
             DataFrame: The input DataFrame with additional 'Scores' column representing the
             calculated anomaly scores.
         """
        scores = []

        # Loop over the predictions, if ground truth outside the PI add distance to score
        for i in range(len(predictions)):
            if predictions["ground_truth"].iloc[i] > predictions[f"1_ahead_{upper_quantile}"].iloc[i]:
                scores.append(predictions["ground_truth"].iloc[i] - predictions[f"1_ahead_{upper_quantile}"].iloc[i])
            elif predictions["ground_truth"].iloc[i] < predictions[f"1_ahead_{lower_quantile}"].iloc[i]:
                scores.append(predictions[f"1_ahead_{lower_quantile}"].iloc[i] - predictions["ground_truth"].iloc[i])
            else:
                scores.append(0.0)

        # Add scores to prediction df
        predictions["Scores"] = scores

        # Average scores over window
        predictions["Scores"] = predictions["Scores"].rolling(window=window).mean()

        return predictions
