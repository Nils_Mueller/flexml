from abc import ABCMeta, abstractmethod
import importlib
import pandas as pd
from darts import TimeSeries
import logging
from pydantic import BaseModel
from core.util import extract_multistep_quantile_predictions, extract_multistep_deterministic_predictions, column_names
from configvalidators.ModelHandlerConfig import ModelHandlerConfig


class ModelHandler(object, metaclass=ABCMeta):
    """
    Abstract base class for handling model configurations, training, and prediction tasks.

    The ModelHandler class provides a framework for managing model parameters, handling
    automatic parameters, and performing model fitting and prediction. It includes methods
    for initializing, training, and forecasting with different types of models while
    abstracting the specifics of model configuration, selection, and evaluation.

    Attributes:
        mdl_cfg (ModelHandlerConfig): Configuration object containing model-specific settings.
        hp (dict): Dictionary containing hyperparameters.
        logtime (str): Time-related string used for logging.
        max_history_window (int): Maximum history window for model training and selection.
        general_auto_params (dict): Automatically generated parameters for model fitting and prediction.
        model_selection_auto_params (dict): Automatically generated parameters for model selection.
    """
    def __init__(self, config, logtime):
        """
        Initializes the ModelHandler object with the provided configuration and logging time.

        Args:
            config (BaseModel or dict): Configuration object or dict with settings for the model.
            logtime (str): Time-related string used for logging.

        Raises:
            TypeError: If the config is not of type BaseModel or dict.
        """
        if isinstance(config, BaseModel):
            self.mdl_cfg = ModelHandlerConfig.from_config(config)
        elif isinstance(config, dict):
            self.mdl_cfg = ModelHandlerConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in ModelHandler. Expected config object or dict.")
        self.hp = self.mdl_cfg.hyperparameters
        self.logtime = logtime
        self.max_history_window = self.set_max_history_window()
        self.general_auto_params = self.get_general_auto_parameter()
        self.model_selection_auto_params = self.get_model_selection_auto_parameter()
        self.check_if_user_defined_auto_params()

    @abstractmethod
    def set_max_history_window(self):
        """
        Abstract method to set the maximum history window for model fitting and prediction.

        This method should return an integer representing the maximum history window
        that the model can use.

        Returns:
            int: Maximum history window.
        """

    def check_if_user_defined_auto_params(self):
        """
        Checks whether user defined any automatically defined parameters.

        If user defined any automatic parameters, a warning is logged and
        the user-defined values for those parameters are removed.

        Raises:
            ValueError: If early stopping is enabled but the number of epochs is defined as a search space.
        """
        auto_params = self.combine_parameter_dicts(self.general_auto_params, self.model_selection_auto_params)
        ignored_user_params = [param for param in self.hp if param in auto_params]
        if any(ignored_user_params):
            warn_msg = f"Parameter(s) {', '.join(ignored_user_params)} automatically defined. User input ignored."
            logging.warning(warn_msg)
            self.hp = {param: val for param, val in self.hp.items() if param not in ignored_user_params}
        if self.mdl_cfg.early_stopping and "n_epochs" in self.hp:
            if isinstance(self.hp["n_epochs"], dict):
                err_msg = ("With early stopping activated, n_epochs is automatically "
                           "optimized and cannot be defined as search space."
                           "In this case, n_epochs serves as maximum.")
                raise ValueError(err_msg)

    def get_predefined_params(self):
        """
        Retrieves the predefined parameters after applying model-specific transformations.

        This method returns the combination of user-defined hyperparameters and general
        automatic parameters, with transformations applied to the hyperparameters specific
        to the model type.

        Returns:
            dict: Combined dictionary of predefined parameters.
        """
        self.hp = self.model_specific_param_transformations(self.hp)
        return self.combine_parameter_dicts(self.hp, self.general_auto_params)

    def get_all_static_model_params_for_prediction(self):
        """
        Retrieves all static model parameters needed for making predictions,
        excluding any parameters that are defined as a search space.

        Returns:
            dict: Combined dictionary of static model parameters for prediction.
        """
        hp_static = {key: value for key, value in self.hp.items() if not isinstance(value, dict)}
        hp_static = self.model_specific_param_transformations(hp_static)
        return self.combine_parameter_dicts(hp_static, self.general_auto_params)

    def get_params_from_search_space(self, trial):
        """
        Selects a set of model hyperparameters for the current optuna trial based on
        the defined search space and returns it together with fixed hyperparameters.

        Args:
            trial (optuna.Trial): An optuna trial object for hyperparameter optimization.

        Returns:
            dict: Set of hyperparameters for the current trial.

        Raises:
            ValueError: If the hyperparameter type is invalid (neither int, float, nor categorical).
        """
        params_cur_trial = {}
        all_hyperparams = self.hp | {"scaling_method": self.mdl_cfg.scaling_method}
        for par_name, par_val in all_hyperparams.items():
            if isinstance(par_val, dict):
                if par_val["type"] == "int":
                    low, high, step = int(par_val["min"]), int(par_val["max"]), par_val.get("step", None)
                    suggested_val = trial.suggest_int(par_name, low, high, **({'step': int(step)} if step else {}))
                elif par_val["type"] == "float":
                    low, high, step = float(par_val["min"]), float(par_val["max"]), par_val.get("step", None)
                    suggested_val = trial.suggest_float(par_name, low, high, **({'step': float(step)} if step else {}))
                elif par_val["type"] == "categorical":
                    choices = par_val["choices"]
                    suggested_val = trial.suggest_categorical(par_name, choices)
                else:
                    raise ValueError("Hyperparameter type must be int, float or categorical.")
            elif isinstance(par_val , (float, int, str)):
                suggested_val = par_val
            else:
                raise ValueError(f"Hyperparameter {par_val} must be float, int, str or dict.")
            params_cur_trial[par_name] = suggested_val
        ms_auto_params = self.get_model_selection_auto_parameter(trial)
        params_cur_trial = self.model_specific_param_transformations(params_cur_trial)
        return self.combine_parameter_dicts(params_cur_trial, ms_auto_params, self.general_auto_params)

    @staticmethod
    def combine_parameter_dicts(*param_dicts) -> dict:
        """
        Combines multiple dictionaries of parameters into a single dictionary and
        also combines nested dicts.

        Args:
            *param_dicts (dict): One or more dictionaries to be combined.

        Returns:
            dict: The combined dictionary of parameters.
        """
        combined_params = {}
        for param_dict in param_dicts:
            for key, value in param_dict.items():
                if key in combined_params and isinstance(combined_params[key], dict) and isinstance(value, dict):
                    combined_params[key].update(value)
                else:
                    combined_params[key] = value
        return combined_params

    @abstractmethod
    def get_general_auto_parameter(self) -> dict:
        """
        Abstract method to retrieve the general auto parameters.

        This method should return a dictionary of general auto parameters
        to be used for model training and evaluation.

        Returns:
            dict: General auto parameters.
        """

    @abstractmethod
    def get_model_selection_auto_parameter(self, trial=None) -> dict:
        """
        Abstract method to retrieve the model selection-specific auto parameters.

        This method should return a dictionary of auto parameters specific to
        model selection (e.g., for hyperparameter optimization).

        Args:
            trial (optuna.Trial, optional): An optuna trial object for hyperparameter optimization.

        Returns:
            dict: Model selection-specific auto parameters.
        """

    @abstractmethod
    def model_specific_param_transformations(self, hp, trial=None) -> dict:
        """
        Abstract method to apply model-specific transformations to the hyperparameters.

        This method should modify the hyperparameters based on model-specific needs or constraints.

        Args:
            hp (dict): Dictionary of hyperparameters.
            trial (optuna.Trial, optional): An optuna trial object for hyperparameter optimization.

        Returns:
            dict: Transformed hyperparameters.
        """

    @abstractmethod
    def get_model_selection_fitting_params(self, trial=None) -> dict:
        """
        Abstract method to retrieve automatic parameters for fitting during model selection.

        Args:
            trial (optuna.Trial, optional): An optuna trial object for hyperparameter optimization.

        Returns:
            dict: Model selection fitting parameters.
        """

    def initialize_model(self, model_params):
        """
        Initializes the model using the provided parameters.

        Args:
            model_params (dict): Dictionary containing model parameters.

        Returns:
            object: The initialized model instance.
        """
        model_module = importlib.import_module("darts.models")
        model_class = getattr(model_module, self.mdl_cfg.type)
        return model_class(**model_params)

    def fit_model_for_model_selection(self, model, x_train, y_train, x_val, y_val, trial):
        """
        Fits the model for model selection, including validation if early stopping or pruning is enabled.

        Args:
            model (object): The model instance to be fitted.
            x_train (pd.DataFrame): Training feature data.
            y_train (pd.Series): Training target data.
            x_val (pd.DataFrame): Validation feature data.
            y_val (pd.Series): Validation target data.
            trial (optuna.Trial): An optuna trial object for hyperparameter optimization.

        Returns:
            object: The fitted model.
        """
        fit_inputs = {"series": TimeSeries.from_dataframe(y_train)}
        if self.mdl_cfg.past_covariates not in [None, []]:
            fit_inputs["past_covariates"] = TimeSeries.from_dataframe(x_train[self.mdl_cfg.past_covariates])
        if  self.mdl_cfg.future_covariates not in [None, []]:
            fit_inputs["future_covariates"] = TimeSeries.from_dataframe(x_train[self.mdl_cfg.future_covariates])
        if self.mdl_cfg.pruning or self.mdl_cfg.early_stopping:
            fit_inputs["val_series"] = TimeSeries.from_dataframe(y_val)
            if self.mdl_cfg.past_covariates not in [None, []]:
                fit_inputs["val_past_covariates"] = TimeSeries.from_dataframe(x_val[self.mdl_cfg.past_covariates])
            if self.mdl_cfg.future_covariates not in [None, []]:
                fit_inputs["val_future_covariates"] = TimeSeries.from_dataframe(x_val[self.mdl_cfg.future_covariates])
        fit_inputs = fit_inputs | self.get_model_selection_fitting_params(trial)
        return model.fit(**fit_inputs)

    def fit_model_for_prediction(self, model, x_train, y_train):
        """
        Fits the model using the provided training data to make predictions.

        Args:
            model (object): The model instance to be fitted.
            x_train (pd.DataFrame): Training feature data.
            y_train (pd.Series): Training target data.

        Returns:
            object: The fitted model.
        """
        logging.info(f"Starting training of {self.mdl_cfg.type}...")
        fit_inputs = {"series": TimeSeries.from_dataframe(y_train)}
        if self.mdl_cfg.past_covariates not in [None, []]:
            fit_inputs["past_covariates"] = TimeSeries.from_dataframe(x_train[self.mdl_cfg.past_covariates])
        if self.mdl_cfg.future_covariates not in [None, []]:
            fit_inputs["future_covariates"] = TimeSeries.from_dataframe(x_train[self.mdl_cfg.future_covariates])
        return model.fit(**fit_inputs)

    def _predict(self, model, x, y, pred_inputs, y_scaler):
        """
        Makes predictions using the provided model and input data.

        This method performs forecasting based on the input features,
        target values, and model configuration. It handles probabilistic
        or deterministic predictions and inverse scaling if required.
        The method also adds ground truth values to the predicted results.

        Args:
            model (object): The model to be used for forecasting.
            x (pd.DataFrame or None): Covariates (can be None if no covariates are provided).
            y (pd.DataFrame): Target data for forecasting.
            pred_inputs (dict): Input parameters for the forecast, including start, retrain, and train length.
            y_scaler (sklearn.preprocessing.StandardScaler or None): Scaler for inverse transformation of predictions.

        Returns:
            pd.DataFrame: DataFrame containing the forecasted values and ground truth.
        """
        pred_inputs.update({"series": TimeSeries.from_dataframe(y),
                            "forecast_horizon": self.mdl_cfg.forecasting_horizon,
                            "verbose": True,
                            "show_warnings": False
                            })
        if self.mdl_cfg.probabilistic:
            pred_inputs["num_samples"] = self.mdl_cfg.num_samples
        if self.mdl_cfg.past_covariates not in [None, []]:
            pred_inputs["past_covariates"] = TimeSeries.from_dataframe(x[self.mdl_cfg.past_covariates])
        if self.mdl_cfg.future_covariates not in [None, []]:
            pred_inputs["future_covariates"] = TimeSeries.from_dataframe(x[self.mdl_cfg.future_covariates])
        if self.mdl_cfg.predict_full_horizon:
            pred_inputs["last_points_only"] = False
        else:
            pred_inputs["last_points_only"] = True

        y_pred = model.historical_forecasts(**pred_inputs)

        ### Store quantiles or deterministic forecasts ###
        columns = column_names(forecasting_horizon=self.mdl_cfg.forecasting_horizon,
                               probabilistic=self.mdl_cfg.probabilistic,
                               quantiles=self.mdl_cfg.quantiles,
                               predict_full_horizon=self.mdl_cfg.predict_full_horizon)

        if self.mdl_cfg.probabilistic:
            y_pred = extract_multistep_quantile_predictions(predictions=y_pred,
                                                            quantiles=self.mdl_cfg.quantiles,
                                                            forecasting_horizon=self.mdl_cfg.forecasting_horizon,
                                                            predict_full_horizon=self.mdl_cfg.predict_full_horizon,
                                                            columns=columns)
        else:
            y_pred = extract_multistep_deterministic_predictions(predictions=y_pred,
                                                                 forecasting_horizon=self.mdl_cfg.forecasting_horizon,
                                                                 predict_full_horizon=self.mdl_cfg.predict_full_horizon,
                                                                 columns=columns)
        # Inverse scaling
        if y_scaler:
            y_pred = pd.DataFrame(y_scaler.inverse_transform(y_pred), columns=columns, index=y_pred.index)

        # Add ground truth
        y_pred["ground_truth"] = y.loc[y_pred.index[0]:y_pred.index[-1]]

        y_pred.reset_index(inplace=True, drop=False)

        return y_pred

    def predict_test_set(self, model, x_train, x_test, y_train, y_test, y_scaler=None):
        """
        Makes predictions on the test set by concatenating the training and test data.

        This method combines the training and test datasets and then generates forecasts
        starting after the training set.

        Args:
            model (object): The model to be used for forecasting.
            x_train (pd.DataFrame): Training feature data.
            x_test (pd.DataFrame): Test feature data.
            y_train (pd.DataFrame): Training target data.
            y_test (pd.DataFrame): Test target data.
            y_scaler (sklearn.preprocessing.StandardScaler or None): Scaler for inverse transformation of predictions.

        Returns:
            pd.DataFrame: DataFrame containing the predicted values on the test set with ground truth.
        """
        logging.info(f"Predicting test data with {self.mdl_cfg.type}...")
        y = pd.concat([y_train, y_test], axis=0)
        if (self.mdl_cfg.past_covariates not in [None, []]) or (self.mdl_cfg.future_covariates not in [None, []]):
            X = pd.concat([x_train, x_test], axis=0)
        else:
            X = None
        pred_inputs = {"start": len(y_train) + self.max_history_window,
                       "retrain": self.mdl_cfg.retrain_dur_prediction,
                       "train_length": self.mdl_cfg.retrain_length_dur_prediction}
        return self._predict(model, X, y, pred_inputs, y_scaler)

    def predict_new_set(self, model, x_hist, x_new, y_hist, y_new, y_scaler=None):
        """
        Makes predictions on a new set of data.

        This method generates forecasts for a new set of data.

        Args:
            model (object): The model to be used for forecasting.
            x_hist (pd.DataFrame): Training feature data.
            x_new (pd.DataFrame): Test feature data.
            y_hist (pd.DataFrame): Training target data.
            y_new (pd.DataFrame): Test target data.
            y_scaler (sklearn.preprocessing.StandardScaler or None): Scaler for inverse transformation of predictions.

        Returns:
            pd.DataFrame: DataFrame containing the predicted values for the new set and ground truth.
        """
        logging.info(f"Predicting new data with {self.mdl_cfg.type}...")
        y = pd.concat([y_hist, y_new], axis=0)
        if (self.mdl_cfg.past_covariates not in [None, []]) or (self.mdl_cfg.future_covariates not in [None, []]):
            X = pd.concat([x_hist, x_new], axis=0)
        else:
            X = None
        pred_inputs = {"start": len(y_hist) + self.max_history_window,
                       "retrain": self.mdl_cfg.retrain_dur_prediction,
                       "train_length": self.mdl_cfg.retrain_length_dur_prediction}
        return self._predict(model, X, y, pred_inputs, y_scaler)

    def predict_val_set(self, model, x_train, x_val, y_train, y_val, y_scaler=None):
        """
        Makes predictions on the validation set.

        This method generates forecasts for the validation set, similar to `predict_new_set`,
        but the prediction process is tailored for validation data and may use different retrain settings.

        Args:
            model (object): The model to be used for forecasting.
            x_train (pd.DataFrame): Training feature data.
            x_val (pd.DataFrame): Test feature data.
            y_train (pd.DataFrame): Training target data.
            y_val (pd.DataFrame): Test target data.
            y_scaler (sklearn.preprocessing.StandardScaler or None): Scaler for inverse transformation of predictions.

        Returns:
            pd.DataFrame: DataFrame containing the predicted values for the validation set and ground truth.
        """
        y = pd.concat([y_train, y_val], axis=0)
        if (self.mdl_cfg.past_covariates not in [None, []]) or (self.mdl_cfg.future_covariates not in [None, []]):
            X = pd.concat([x_train, x_val], axis=0)
        else:
            X = None
        pred_inputs = {"start": len(y_train)+self.max_history_window,
                       "retrain": self.mdl_cfg.retrain_dur_selection,
                       "train_length": self.mdl_cfg.retrain_length_dur_selection}
        return self._predict(model, X, y, pred_inputs, y_scaler)
