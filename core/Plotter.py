from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
from optuna.visualization.matplotlib import plot_param_importances, plot_optimization_history
import optuna
import matplotlib
from darts.explainability import ShapExplainer
import logging
import warnings
import os
import pandas as pd
warnings.filterwarnings("ignore", category=optuna.exceptions.ExperimentalWarning)
from pydantic import BaseModel
from configvalidators.PlotterConfig import PlotterConfig

class Plotter:
    """
    A class responsible for generating and saving various plots related to model predictions,
    explainability, and optimization, including deterministic and probabilistic predictions,
    anomaly detection visualizations, SHAP explainability plots, and hyperparameter
    optimization history.

    Attributes:
        pl_cfg (PlotterConfig): Configuration object containing the settings for plotting.
        DIR_TEST_SET_PRED_PLOT (str): Directory for saving test set prediction plots.
        DIR_NEW_SET_PRED_PLOT (str): Directory for saving new set prediction plots.
        DIR_TEST_SET_SHAP_PLOT (str): Directory for saving SHAP explainer plots for the test set.
        DIR_PARAM_IMPORTANCES_PLOT (str): Directory for saving parameter importance plots.
        DIR_OPTIMIZATION_HIST_PLOT (str): Directory for saving optimization history plots.
        logtime (float): Timestamp used for saving plot files.
    """
    DIR_TEST_SET_PRED_PLOT = os.path.join("results", "plots", "predictions", "test_set")
    DIR_NEW_SET_PRED_PLOT = os.path.join("results", "plots", "predictions", "new_set")
    DIR_TEST_SET_SHAP_PLOT = os.path.join("results", "plots", "shap_explainer", "test_set")
    DIR_PARAM_IMPORTANCES_PLOT = os.path.join("results", "plots", "parameter_importances")
    DIR_OPTIMIZATION_HIST_PLOT = os.path.join("results", "plots", "optimization_history")

    def __init__(self, config, logtime):
        """
        Initializes the Plotter class with the provided configuration and timestamp.

        Args:
            config (BaseModel or dict): Configuration for the plotter. Can either be a BaseModel object
                                        or a dictionary that can be converted into a configuration.
            logtime (float): Timestamp used for logging and file naming.
        """
        if isinstance(config, BaseModel):
            self.pl_cfg = PlotterConfig.from_config(config)
        elif isinstance(config, dict):
            self.pl_cfg = PlotterConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in Plotter. Expected config object or dict.")
        self.logtime = logtime
        if self.pl_cfg.steps_to_plot in [None, []]:
            self.pl_cfg.steps_to_plot = [v+1 for v in range(self.pl_cfg.forecasting_horizon)]

    def _plot_predictions(self, y_pred, step_ahead, title, target_feature, anomaly_detection, quantiles,
                                 probabilistic, anomaly_data=None, extra_subplots=False):
        """
        Plots the predictions (either probabilistic or deterministic) along with optional anomaly
        detection visualizations.

        Args:
            y_pred (pd.DataFrame): Dataframe containing the predictions.
            step_ahead (int): The forecast step ahead.
            title (str): The title of the plot.
            target_feature (str): The target feature being predicted.
            anomaly_detection (bool): Whether anomaly detection is enabled.
            quantiles (list): The quantiles for probabilistic predictions.
            probabilistic (bool): Whether the predictions are probabilistic.
            anomaly_data (list, optional): Anomaly data to highlight on the plot.
            extra_subplots (bool, optional): Whether to add extra subplots for anomaly details.

        Returns:
            fig, ax: The figure and axis objects of the plot.
        """
        plt.style.use('seaborn-v0_8-whitegrid')

        if probabilistic:
            # Plot probabilistic predictions
            height, n_rows = (12, 3) if anomaly_detection and extra_subplots else (8, 2) if anomaly_detection else (
            4, 1)
            fig, ax = plt.subplots(nrows=n_rows, ncols=1, figsize=(12, height), sharex=True)
            ax = [ax] if n_rows == 1 else ax

            ax[0].plot(y_pred["Timestamps"], y_pred[f"{step_ahead}_ahead_0.5"], color="blue", label="Median Prediction")
            ax[0].fill_between(y_pred["Timestamps"],
                               y_pred[f"{step_ahead}_ahead_{quantiles[0]}"],
                               y_pred[f"{step_ahead}_ahead_{quantiles[-1]}"],
                               alpha=0.1, color="blue",
                               label=f"{100 * (1 - 2 * quantiles[0]):.0f}% Prediction Interval")

            if anomaly_detection:
                if anomaly_data:  # Handle anomaly annotations for new data
                    for anomaly in anomaly_data:
                        ax[0].axvspan(anomaly["Start"], anomaly["End"], alpha=0.1, color='red')
                        ax[0].text(pd.Timestamp(anomaly["Start"]), 1.1,
                                   f"{anomaly['Type']} {anomaly['Victim']}\n {anomaly['Reasoning']}", rotation=90,
                                   fontsize=5)
                ax[1].plot(y_pred["Timestamps"], y_pred["Scores"], color="black", label="Anomaly Score")
                ax[1].set_ylabel("Anomaly Score", fontsize=11)
                ax[1].hlines(y=self.pl_cfg.threshold, linestyle="dashed", xmin=y_pred["Timestamps"].iloc[0],
                             xmax=y_pred["Timestamps"].iloc[-1], label="Threshold")
                ax[1].legend()

                if extra_subplots:
                    ax[2].plot(y_pred["Timestamps"].astype("datetime64[ns]"), y_pred["Flag_direction"], color="red",
                               label="Anomaly type")
                    ax[2].set_yticks((-2, -1, 0, 1, 2))
                    ax[2].set_yticklabels(('Low|zero', 'Low', 'Normal', 'High', 'High|zero'))
                    ax[2].set_ylabel('Anomaly type')

                ax[2 if extra_subplots else 1].set_xlabel("Time", fontsize=11)
            else:
                ax[0].set_xlabel("Time", fontsize=11)
        else:
            # Plot deterministic predictions
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 4), sharex=True)
            ax = [ax]
            ax[0].set_xlabel("Time", fontsize=11)
            ax[0].plot(y_pred["Timestamps"], y_pred[f"{step_ahead}_ahead"], color="blue", label="Prediction")
            if anomaly_detection and anomaly_data:
                for anomaly in anomaly_data:
                    ax[0].axvspan(anomaly["Start"], anomaly["End"], alpha=0.1, color='red')
                    ax[0].text(pd.Timestamp(anomaly["Start"]), 1.1,
                               f"{anomaly['Type']} {anomaly['Victim']}\n {anomaly['Reasoning']}", rotation=90,
                               fontsize=5)

        ax[0].set_ylabel(target_feature, fontsize=11)
        ax[0].plot(y_pred["Timestamps"], y_pred["ground_truth"], color="black", label="Ground truth")
        ax[0].legend()

        # Customize axis
        for axis in ax:
            axis.set_xlim([y_pred["Timestamps"].min(), y_pred["Timestamps"].max()])
            axis.tick_params(axis='both', which='major', labelsize=11)
            axis.tick_params(axis='both', which='both', length=0)
            axis.xaxis.set_major_locator(MaxNLocator(nbins=5))
            for spine in axis.spines.values():
                spine.set_visible(True)
                spine.set_edgecolor('black')
                spine.set_linewidth(0.7)

        fig.suptitle(title, fontweight='bold')
        plt.tight_layout()

        return fig, ax

    def plot_test_predictions(self, y_pred_test):
        """
        Plots the predictions for the test set and saves them as SVG files.

        Args:
            y_pred_test (pd.DataFrame): Dataframe containing the predicted values for the test set.
        """
        anomaly_detection = self.pl_cfg.anomaly_detection
        quantiles = self.pl_cfg.quantiles
        target_feature = self.pl_cfg.target_feature
        for step_ahead in self.pl_cfg.steps_to_plot:
            title = f'{step_ahead}-steps ahead probabilistic prediction (PI={1 - 2 * quantiles[0]}) of the test set' if self.pl_cfg.probabilistic else f'{step_ahead}-steps ahead deterministic prediction'
            fig, ax = self._plot_predictions(y_pred_test, step_ahead, title, target_feature, anomaly_detection,
                                             quantiles, self.pl_cfg.probabilistic)

            filename = f"{self.logtime}_{self.pl_cfg.type}_{step_ahead}ahead_{self.pl_cfg.Name}.svg"
            plt.savefig(os.path.join(self.DIR_TEST_SET_PRED_PLOT, filename), format="svg")
            plt.close()

    def plot_new_predictions(self, y_pred_new):
        """
        Plots the predictions for the new data set and saves them as SVG files,
        including anomaly detection.

        Args:
            y_pred_new (pd.DataFrame): Dataframe containing the predicted values for the new data set.
        """
        anomaly_detection = self.pl_cfg.anomaly_detection
        quantiles = self.pl_cfg.quantiles
        target_feature = self.pl_cfg.target_feature
        for step_ahead in self.pl_cfg.steps_to_plot:
            title = f'{step_ahead}-steps ahead probabilistic prediction (PI={1 - 2 * quantiles[0]}) of the new set' if self.pl_cfg.probabilistic else f'{step_ahead}-steps ahead deterministic prediction'
            fig, ax = self._plot_predictions(y_pred_new, step_ahead, title, target_feature, anomaly_detection,
                                             quantiles, self.pl_cfg.probabilistic,
                                             anomaly_data=self.pl_cfg.Anomaly_ground_truth,
                                             extra_subplots=True)
            filename = f"{self.logtime}_{self.pl_cfg.type}_{step_ahead}ahead_{self.pl_cfg.Name}.svg"
            plt.savefig(os.path.join(self.DIR_NEW_SET_PRED_PLOT, filename), format="svg")
            plt.close()

    def plot_shap_explainer(self, model):
        """
        Plots SHAP (SHapley Additive exPlanations) explainability summaries for the model.

        Args:
            model (object): The trained model for which SHAP explainability plots will be generated.
        """
        warnings.filterwarnings("ignore", message="FigureCanvasAgg is non-interactive, and thus cannot be shown")
        shap_logger = logging.getLogger('darts.explainability.shap_explainer')
        shap_logger.setLevel(logging.WARNING)
        current_backend = matplotlib.get_backend()
        matplotlib.use('Agg')
        explainer = ShapExplainer(model=model)
        for step_ahead in self.pl_cfg.steps_to_plot:
            explainer.summary_plot(horizons=step_ahead)
            filename = f"{self.logtime}_{self.pl_cfg.type}_{step_ahead}ahead_{self.pl_cfg.Name}.svg"
            plt.savefig(os.path.join(self.DIR_TEST_SET_SHAP_PLOT, filename), format="svg")
            plt.close()
        matplotlib.use(current_backend)

    def save_hyperparameter_importances_plot(self, study):
        """
        Saves a plot of hyperparameter importances based on an Optuna study.

        Args:
            study (optuna.study.Study): The Optuna study object containing the optimization results.
        """
        plot_param_importances(study)
        filename = f"{self.logtime}_{self.pl_cfg.type}_{self.pl_cfg.Name}.svg"
        plt.tight_layout()
        plt.savefig(os.path.join(self.DIR_PARAM_IMPORTANCES_PLOT, filename), format="svg")
        plt.close()

    def save_optimization_hist_plot(self, study):
        """
        Saves a plot of the optimization history based on an Optuna study.

        Args:
            study (optuna.study.Study): The Optuna study object containing the optimization history.
        """
        plot_optimization_history(study)
        filename = f"{self.logtime}_{self.pl_cfg.type}_{self.pl_cfg.Name}.svg"
        plt.tight_layout()
        plt.savefig(os.path.join(self.DIR_OPTIMIZATION_HIST_PLOT, filename), format="svg")
        plt.close()
