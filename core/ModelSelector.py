import importlib
import logging
import optuna
import os
import numpy as np
from optuna.study import MaxTrialsCallback
from optuna.trial import TrialState
from sklearn.model_selection import TimeSeriesSplit
import warnings
warnings.filterwarnings("ignore", category=FutureWarning, module="darts")
from pydantic import BaseModel
from configvalidators.ModelSelectorConfig import ModelSelectorConfig

class ModelSelector:
    """
    A class for selecting the optimal model parameters using hyperparameter optimization in Optuna.

    Attributes:
        ms_cfg (ModelSelectorConfig): Configuration object containing settings for model selection.
        model_handler (object): An instance responsible for model management (e.g., fitting, prediction).
        data_preprocessor (object): An instance responsible for data preprocessing (e.g., scaling, splitting).
        plotter (object): An instance for plotting and saving visualizations related to model selection.
        result_storer (object): An instance for storing and saving results (e.g., optimal parameters, plots).
        model_evaluator (object): An instance responsible for evaluating model performance (e.g., error calculation).
        logtime (str): A timestamp used for logging purposes.
        model_class (type): The model class to be optimized.
        X_train_sets (list): List of covariate training sets.
        X_val_sets (list): List of covariate validation sets.
        y_train_sets (list): List of target feature training sets.
        y_val_sets (list): List of target feature validation sets.
    """
    def __init__(self, config, model_handler, data_preprocessor, model_evaluator, plotter, result_storer, logtime):
        """
        Initializes the ModelSelector with the provided configuration and associated components.

        Args:
            config (BaseModel or dict): Configuration object or dictionary containing settings for the model selection.
            model_handler (object): An instance for handling models, including fitting and predicting.
            data_preprocessor (object): An instance for handling data preprocessing (e.g., splitting, scaling).
            model_evaluator (object): An instance for evaluating the model's performance.
            plotter (object): An instance for visualizing and saving plots related to model selection.
            result_storer (object): An instance for storing results such as optimized hyperparameters and plots.
            logtime (str): The timestamp used in logging.
        """
        if isinstance(config, BaseModel):
            self.ms_cfg = ModelSelectorConfig.from_config(config)
        elif isinstance(config, dict):
            self.ms_cfg = ModelSelectorConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in ModelSelector. Expected config object or dict.")
        self.model_handler = model_handler
        self.data_preprocessor = data_preprocessor
        self.plotter = plotter
        self.result_storer = result_storer
        self.model_evaluator = model_evaluator
        self.logtime = logtime
        self.model_class = None
        self.X_train_sets, self.X_val_sets, self.y_train_sets, self.y_val_sets = [], [], [], []

    def determine_optimal_params(self, x_hist, y_hist):
        """
        Determines the optimal hyperparameters for the model Optuna.

        This method splits the historical data into training and validation sets,
        initializes the model, and then runs Optuna optimization to find the optimal
        hyperparameters for the model.

        Args:
            x_hist (pd.DataFrame): The covariates of the historical dataset.
            y_hist (pd.Series): The target feature of the historical dataset.

        Returns:
            tuple: A tuple containing the full model parameter set (static and optimized) and the scaling method.
        """
        logging.info(f"Starting hyperparameter optimization of {self.ms_cfg.type}...")
        # Create sets of training/validation data
        X_training, _, y_training, _ = self.data_preprocessor.split_data_into_training_test(x_hist, y_hist)
        if self.ms_cfg.CV_splits > 1:
            tscv = TimeSeriesSplit(n_splits=self.ms_cfg.CV_splits)
            for train_idx, val_idx in tscv.split(y_training):
                self.X_train_sets.append(X_training.iloc[train_idx])
                self.X_val_sets.append(X_training.iloc[val_idx])
                self.y_train_sets.append(y_training.iloc[train_idx])
                self.y_val_sets.append(y_training.iloc[val_idx])
        else:
            X_train, X_val, y_train, y_val = self.data_preprocessor.split_data_into_train_val(X_training, y_training)
            self.X_train_sets.append(X_train)
            self.X_val_sets.append(X_val)
            self.y_train_sets.append(y_train)
            self.y_val_sets.append(y_val)

        # Load model class
        self.model_class = self.load_model_class()

        # Set up study name and sql storage link for running optuna on multiple machines
        study_name = f"{self.ms_cfg.type}_{self.ms_cfg.Name}"
        storage_link = f"sqlite:///{os.path.join('optuna_sql_databases', f'{study_name}.db')}"

        # Define the study
        study = optuna.create_study(direction="minimize",
                                    study_name=study_name if self.ms_cfg.selection_on_multiple_machines else None,
                                    storage=storage_link if self.ms_cfg.selection_on_multiple_machines else None,
                                    load_if_exists=True)

        # Run the optimization
        study.optimize(self.objective,
                       n_trials=self.ms_cfg.n_trials,
                       callbacks=[MaxTrialsCallback(self.ms_cfg.n_trials, states=(TrialState.COMPLETE,))],
                       timeout=self.ms_cfg.timeout)

        best_trial = study.best_trial

        # Get best varied parameter from study
        best_varied_params = dict(study.best_params)
        if self.ms_cfg.early_stopping:
            best_varied_params["n_epochs"] = best_trial.user_attrs['actual_epochs']
        best_varied_params = self.model_handler.model_specific_param_transformations(best_varied_params)

        # Print optimal params
        logging.info(f"Optimal values of the varied hyperparameters found to be: {best_varied_params}")

        # Save parameter importances
        if self.ms_cfg.save_parameter_importances:
            self.result_storer.save_hyperparameter_importances(study)

        # Save plot of parameter importances
        if self.ms_cfg.save_parameter_importances_plot:
            self.plotter.save_hyperparameter_importances_plot(study)

        # Save hyperparameter optimization history
        if self.ms_cfg.save_optimization_history_plot:
            self.plotter.save_optimization_hist_plot(study)

        # Get all static model params not included in hyperparameter selection
        static_params = self.model_handler.get_all_static_model_params_for_prediction()

        # Save optimal and fixed hyperparameters
        if self.ms_cfg.save_optimal_hyperparameters:
            self.result_storer.save_optimal_hyperparams(best_varied_params, static_params)

        # Get scaling method, either from optimization if included or from config, and remove from best params if there
        scaling_method = best_varied_params.pop("scaling_method", self.ms_cfg.scaling_method)

        # Combine static and varied params to get full model parameter set
        full_model_params = self.model_handler.combine_parameter_dicts(static_params, best_varied_params)

        return full_model_params, scaling_method

    def load_model_class(self):
        """
        Dynamically loads the model class specified in the configuration.

        Returns:
            type: The model class corresponding to the type specified in the configuration.
        """
        model_module = importlib.import_module(f"darts.models")
        return getattr(model_module, self.ms_cfg.type)

    def objective(self, trial):
        """
        The objective function used by Optuna to evaluate model performance for a given trial.

        This method generates model configurations from the trial, trains the model,
        and computes the error on the validation set. It returns the average error for
        the optimization process.

        Args:
            trial (optuna.trial.Trial): The current trial being evaluated by Optuna.

        Returns:
            float: The average error across all steps ahead in the validation set.
        """
        trial_params = self.model_handler.get_params_from_search_space(trial)

        # Set up the respective model
        trial_model = self.model_class(**{p: trial_params[p] for p in trial_params if p not in ["scaling_method"]})

        # Calculate errors for each fold
        errors = []
        for X_train, X_val, y_train, y_val in zip(self.X_train_sets, self.X_val_sets, self.y_train_sets, self.y_val_sets):

            # Scale data (must be done within objective function as scaler can be different in each trial)
            X_train, X_val, y_train, y_val, y_scaler = self.data_preprocessor.scale_data(x_train=X_train,
                                                                                         x_test=X_val,
                                                                                         y_train=y_train,
                                                                                         y_test=y_val,
                                                                                         scaling_method=trial_params["scaling_method"])

            # Fit model to historical train data scaled with scaler of current trial
            trial_model = self.model_handler.fit_model_for_model_selection(trial_model, X_train, y_train, X_val, y_val, trial)

            # Save actual optimal number of epochs if early stopping applied:
            if self.ms_cfg.early_stopping:
                trial_epochs = trial_model.epochs_trained
                trial_model = trial_model.load_from_checkpoint(model_name=f"{self.logtime}_{self.ms_cfg.type}", best=True)
                trial_epochs = trial_epochs - self.ms_cfg.es_patience
                trial.set_user_attr('actual_epochs', trial_epochs)

            # Predict validation data
            y_pred_val = self.model_handler.predict_val_set(trial_model, X_train, X_val, y_train, y_val, y_scaler)

            # Calculate error for each step ahead
            err_fold = self.model_evaluator.calculate_error_for_each_step_ahead(y_pred_val, y_pred_val["ground_truth"])

            # Save errors of the current fold to Errors list
            errors.append(err_fold)

        # Average error of each step ahead over the folds
        errors_each_step_ahead = np.mean(errors, axis=0)

        # Return average error over all steps ahead
        return self.model_evaluator.average_error_of_all_steps_ahead(errors_each_step_ahead)
