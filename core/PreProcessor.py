from sklearn.model_selection import train_test_split
import pandas as pd
from core.util import scaler_initialization
from pydantic import BaseModel, field_validator
from configvalidators.PreProcessorConfig import PreProcessorConfig

class PreProcessor:
    """
    A class responsible for preprocessing time series data, including tasks like
    resampling, interpolation, splitting into training and test sets, differencing,
    and scaling of covariates and the target feature.

    Attributes:
        pp_cfg (PreProcessorConfig): Configuration object containing the settings for data preprocessing.
    """
    def __init__(self, config):
        """
        Initializes the PreProcessor with the provided configuration.

        Args:
            config (BaseModel or dict): Configuration for the data preprocessing.
                                        Can either be a BaseModel object or a dictionary
                                        that can be converted into a configuration.
        """
        if isinstance(config, BaseModel):
            self.pp_cfg = PreProcessorConfig.from_config(config)
        elif isinstance(config, dict):
            self.pp_cfg = PreProcessorConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in PreProcessor. Expected config object or dict.")


    def preprocess_data(self, data):
        """
        Preprocesses the input data according to the configuration settings, including interpolation,
        resampling, differencing of features, and separating the target variable.

        Args:
            data (pd.DataFrame): The input time series data to be preprocessed.

        Returns:
            X (pd.DataFrame): The covariates.
            y (pd.DataFrame): The target feature.
        """

        # Interpolate
        if self.pp_cfg.interpolate:
            data = data.interpolate()

        # Resample
        if self.pp_cfg.resample:
            data = data.resample(rule=self.pp_cfg.resampling_rule).mean()

        # Separate target variable (y) and covariates (X)
        X_past = data[self.pp_cfg.past_covariates] if self.pp_cfg.past_covariates else data[[]]
        X_future = data[self.pp_cfg.future_covariates] if self.pp_cfg.future_covariates else data[[]]
        y = data[[self.pp_cfg.target_feature]]

        # Difference
        if self.pp_cfg.difference_past_covariates:
            X_past = X_past.diff()
        if self.pp_cfg.difference_future_covariates:
            X_future = X_future.diff()
        if self.pp_cfg.difference_target:
            y = y.diff()
        if self.pp_cfg.difference_target or self.pp_cfg.difference_past_covariates or self.pp_cfg.difference_future_covariates:
            y, X_past, X_future = y.iloc[1:], X_past.iloc[1:], X_future.iloc[1:]

        # Merge covariates and drop duplicates
        X = pd.concat([X_past, X_future], axis=1)
        X = X.loc[:, ~X.columns.duplicated()]

        return X, y

    def split_data_into_training_test(self, X, y):
        """
        Splits the historical data into training and testing sets based on the configuration settings.

        Args:
            X (pd.DataFrame): Covariates
            y (pd.DataFrame): Target feature

        Returns:
            tuple: A tuple containing the training and testing data (X_train, X_test, y_train, y_test).
        """
        return train_test_split(X, y, train_size=self.pp_cfg.hist_data_training_size, shuffle=False)

    def split_data_into_train_val(self, X, y):
        """
        Splits the training part of historical data into training and validation sets
        based on the configuration settings.

        Args:
            X (pd.DataFrame): Covariates
            y (pd.DataFrame): Target feature

        Returns:
            tuple: A tuple containing the training and validation data (X_train, X_val, y_train, y_val).
        """
        return train_test_split(X, y, train_size=self.pp_cfg.train_size, shuffle=False)

    def scale_data(self, x_train, x_test, y_train, y_test, scaling_method):
        """
        Scales the input data using the specified scaling method.

        Args:
            x_train (pd.DataFrame): Covariates for training.
            x_test (pd.DataFrame): Covariates for testing.
            y_train (pd.DataFrame): Target feature for training.
            y_test (pd.DataFrame): Target feature for testing.
            scaling_method (str): The scaling method to be used (e.g., 'minmax', 'standard').

        Returns:
            tuple: A tuple containing the scaled training and testing data,
            and the scaler used for target variable (if applicable).
        """
        y_scaler = None
        if self.pp_cfg.future_covariates or self.pp_cfg.past_covariates:
            x_scaler = scaler_initialization(scaling_method)
            x_train = pd.DataFrame(x_scaler.fit_transform(x_train), index=x_train.index, columns=x_train.columns)
            x_test = pd.DataFrame(x_scaler.transform(x_test), index=x_test.index, columns=x_test.columns)
        if self.pp_cfg.scale_y:
            y_scaler = scaler_initialization(scaling_method)
            y_train = pd.DataFrame(y_scaler.fit_transform(y_train), index=y_train.index, columns=y_train.columns)
            y_test = pd.DataFrame(y_scaler.transform(y_test), index=y_test.index, columns=y_test.columns)
        return x_train, x_test, y_train, y_test, y_scaler
