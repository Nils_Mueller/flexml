import os
import pprint
import json
from optuna.importance import get_param_importances
import pandas as pd
from pydantic import BaseModel
from configvalidators.ResultStorerConfig import ResultStorerConfig


class ResultStorer:
    """
    A class for storing model results, including predictions, hyperparameter importances,
    and evaluation metrics for test and new datasets. The results are saved to predefined
    directories based on model type, experiment name and the current timestamp.

    Attributes:
        DIR_TEST_SET_PRED (str): Directory for saving test set predictions.
        DIR_NEW_SET_PRED (str): Directory for saving new set predictions.
        DIR_TEST_SET_METRICS (str): Directory for saving test set metrics.
        DIR_NEW_SET_METRICS (str): Directory for saving new set metrics.
        DIR_PARAM_IMPORTANCES (str): Directory for saving hyperparameter importances.
        DIR_OPTIMAL_HYPERPARAMS (str): Directory for saving optimal hyperparameters.
        rs_cfg (ResultStorerConfig): The configuration object containing result storage settings.
        name (str): Name identifier for the current experiment.
        logtime (str): Timestamp of the current experiment.
    """
    DIR_TEST_SET_PRED = os.path.join("results", "predictions", "test_set")
    DIR_NEW_SET_PRED = os.path.join("results", "predictions", "new_set")
    DIR_TEST_SET_METRICS = os.path.join("results", "metrics", "test_set")
    DIR_NEW_SET_METRICS = os.path.join("results", "metrics", "new_set")
    DIR_PARAM_IMPORTANCES = os.path.join("results", "models", "parameter_importances")
    DIR_OPTIMAL_HYPERPARAMS = os.path.join("results", "models", "optimal_hyperparameters")

    def __init__(self, config, logtime):
        """
         Initializes the ResultStorer object.

         Args:
             config (BaseModel or dict): Configuration object or dictionary used to initialize the
                                         ResultStorerConfig.
             logtime (str): Timestamp to distinguish results.

         Raises:
             TypeError: If the config is neither a BaseModel nor a dict.
         """
        if isinstance(config, BaseModel):
            self.rs_cfg = ResultStorerConfig.from_config(config)
        elif isinstance(config, dict):
            self.rs_cfg = ResultStorerConfig.from_dict(config)
        else:
            raise TypeError("Invalid config type in ResultStorer. Expected config object or dict.")
        self.name = self.rs_cfg.Name
        self.logtime = logtime

    def save_test_set_predictions(self, y_pred_test):
        """
        Saves the test set predictions to a CSV file.

        Args:
            y_pred_test (pd.DataFrame): DataFrame containing test set predictions.
        """
        self._save_predictions(y_pred_test, self.DIR_TEST_SET_PRED)

    def save_new_set_predictions(self, y_pred_new):
        """
        Saves the new set predictions to a CSV file.

        Args:
            y_pred_new (pd.DataFrame): DataFrame containing new set predictions.
        """
        self._save_predictions(y_pred_new, self.DIR_NEW_SET_PRED)

    def _save_predictions(self, y_pred, directory):
        """
        Saves the provided predictions to a CSV file in the specified directory.

        Args:
            y_pred (pd.DataFrame): DataFrame containing predictions.
            directory (str): Directory where the predictions should be saved.
        """
        filename = f"{self.logtime}_{self.rs_cfg.type}_{self.name}.csv"
        y_pred.to_csv(os.path.join(directory, filename), index=False)

    def save_hyperparameter_importances(self, study):
        """
        Saves the hyperparameter importances to a CSV file.

        Args:
            study: The Optuna study object used to calculate the hyperparameter importances.
        """
        param_importances = get_param_importances(study)
        importances_df = pd.DataFrame(param_importances.items(), columns=['Hyperparameter', 'Importance'])
        filename = f"{self.logtime}_{self.rs_cfg.type}_{self.name}.csv"
        importances_df.to_csv(os.path.join(self.DIR_PARAM_IMPORTANCES, filename), index=False)

    def save_optimal_hyperparams(self, best_params, fixed_params):
        """
        Saves the optimal and fixed hyperparameters to a text file.

        Args:
            best_params (dict): The best hyperparameters found.
            fixed_params (dict): The hyperparameters that were fixed during the experiment.
        """
        filename = f"{self.logtime}_{self.rs_cfg.type}_{self.name}.txt"
        params = {"Optimal_hyperparameters": best_params, "Fixed_params": fixed_params}
        formatted_dict = pprint.pformat(params)
        with open(os.path.join(self.DIR_OPTIMAL_HYPERPARAMS, filename), 'w', encoding='utf-8') as f:
            f.write(formatted_dict)

    def _create_metrics_dict(self, rmse=None, mae=None, pinball=None, custom=None):
        """
        Creates a dict of evaluation metrics, including RMSE, MAE, Pinball, and custom metrics.

        Args:
            rmse (list, optional): List of RMSE values.
            mae (list, optional): List of MAE values.
            pinball (list, optional): List of Pinball loss values.
            custom (list, optional): List of custom metric values.

        Returns:
            dict: A dictionary containing the metrics per step ahead and their averages.
        """
        if self.rs_cfg.predict_full_horizon:
            steps_ahead = list(range(1, self.rs_cfg.forecasting_horizon + 1))
        else:
            steps_ahead = [self.rs_cfg.forecasting_horizon]

        metrics = {}

        if rmse is not None:
            rmse_avg = sum(rmse) / len(rmse)
            rmse = {f"{steps_ahead[idx]}_ahead": rmse[idx] for idx in range(len(rmse))}
            metrics["RMSE"] = {"Per_step_ahead": rmse, "Average": rmse_avg}

        if mae is not None:
            mae_avg = sum(mae) / len(mae)
            mae = {f"{steps_ahead[idx]}_ahead": mae[idx] for idx in range(len(mae))}
            metrics["MAE"] = {"Per_step_ahead": mae, "Average": mae_avg}

        if pinball is not None:
            pinball_avg = sum(pinball) / len(pinball)
            pinball = {f"{steps_ahead[idx]}_ahead": pinball[idx] for idx in range(len(pinball))}
            metrics["PINBALL"] = {"Per_step_ahead": pinball, "Average": pinball_avg}

        if custom is not None:
            custom_avg = sum(custom) / len(custom)
            custom = {f"{steps_ahead[idx]}_ahead": custom[idx] for idx in range(len(custom))}
            metrics["CUSTOM"] = {"Per_step_ahead": custom, "Average": custom_avg}

        return metrics

    def save_test_set_metrics(self, rmse=None, mae=None, pinball=None, custom=None):
        """
        Saves the test set evaluation metrics to a JSON file.

        Args:
            rmse (list, optional): List of RMSE values.
            mae (list, optional): List of MAE values.
            pinball (list, optional): List of Pinball loss values.
            custom (list, optional): List of custom metric values.
        """
        metrics = self._create_metrics_dict(rmse, mae, pinball, custom)
        filename = f"{self.logtime}_{self.rs_cfg.type}_{self.name}.json"
        with open(os.path.join(self.DIR_TEST_SET_METRICS, filename), 'w', encoding='utf-8') as f:
            json.dump(metrics, f, indent=4)

    def save_new_set_metrics(self, rmse=None, mae=None, pinball=None, custom=None):
        """
        Saves the new set evaluation metrics to a JSON file.

        Args:
            rmse (list, optional): List of RMSE values.
            mae (list, optional): List of MAE values.
            pinball (list, optional): List of Pinball loss values.
            custom (list, optional): List of custom metric values.
        """
        metrics = self._create_metrics_dict(rmse, mae, pinball, custom)
        filename = f"{self.logtime}_{self.rs_cfg.type}_{self.name}.json"
        with open(os.path.join(self.DIR_NEW_SET_METRICS, filename), 'w', encoding='utf-8') as f:
            json.dump(metrics, f, indent=4)
