Name: example_anomaly_detection  # Name of the experiment (will be included in result file names) (mandatory)
Comment: This is a config file for exemplifying the use of FlexML  # A comment to describe the experiment (mandatory)

Pipeline:
  model_selection: False  # Boolean to (de)activate model selection (mandatory)
  predict_test_data: True  # Boolean to (de)activate test set prediction (mandatory)
  predict_new_data: True  # Boolean to (de)activate new set prediction (mandatory)
  anomaly_detection: True  # Boolean to (de)activate anomaly detection (mandatory)

Datasets:
  hist_data_filename: "historical_data_example.csv"  # Filename of the historical data set in csv format (mandatory)
  new_data_filename: "new_data_example.csv" # Filename of the new data set in csv format (optional)
  try_out_dataset: False  # Fraction (0-1) of historical and/or new data to be used. Can also be set False (optional)
  hist_data_training_size: 0.8  # Fraction (0-1) of historical data used for model selection/(re)training (mandatory)
  target_feature: 'PV_feed[kW]'  # Target feature to be predicted. Must be available in historical and new data (mandatory)
  future_covariates: ["Irradiance_forecast[W/m2]"] # Covariates known for the future such as time of day. Must be a list or None (optional)
  past_covariates:  # Covariates only known for the past, e.g. power measurements. Must be a list or None (optional)

Preprocessing:
  interpolate: True  # Boolean to (de)activate linear interpolation of missing data (mandatory)
  resample: False  # Boolean to (de)activate resampling of the data (mandatory)
  resampling_rule: "1min"  # Frequency for resampling data. Check Pandas Doc for possible frequencies (mandatory)
  difference_target: False  # Boolean to (de)activate differencing the target feature (mandatory)
  difference_future_covariates: False  # Boolean to (de)activate differencing the future covariates (mandatory)
  difference_past_covariates: False  # Boolean to (de)activate differencing the past covariates (mandatory)
  scaling_method: "minmax"  # minmax, standard, no_scaling or as space {choices:['minmax'],type:categorical} (mandatory)
  scale_y: False  # Boolean to (de)activate scaling of the target feature (mandatory)

Model:
  type: LightGBMModel  # Model type. Can be any of the ones with existing handler in the modelhandlers folder (mandatory)
  forecasting_horizon: 3  # How many steps to be predicted into the future (mandatory)
  predict_full_horizon: True  # Boolean to decide if all steps or only the furthest should be predicted (mandatory)
  probabilistic: True  # Boolean to (de)activate probabilistic predictions. Not supported by all models. (mandatory)
  num_samples: 1000  # Number of samples to be drawn if probabilistic predictions. Must be >1 if probabilistic (optional)
  probability_model: "quantile_reg"  # Probability model to be used. "quantile_reg" "gaussian" or "laplace" (optional)
  quantiles: [ 0.1, 0.5, 0.9 ]  # Quantiles to be predicted if probabilistic (optional)
  hyperparameters:  # Model-specific hyperparameters. Check Darts and Model doc for available parameter (mandatory)
    lags: 48  # Params can be defined as fixed value or search space, e.g., {min: 1, max: 5, step: 1, type: int}
    verbose: -1

Model_selection:
  n_trials: 5  # Number of parameter combinations from the defined search space to be tried (mandatory)
  timeout: 3600  # Timeout in seconds to interrupt hyperparameter study (mandatory)
  CV_splits: 1  # Number of train/val splits. If >1, time-series cross-validation applied (mandatory)
  train_size: 0.6  # Size of training set during model selection. Only relevant if no time-series CV used (mandatory)
  error_metric: "RMSE"  # 'RMSE','MAE','PINBALL','CUSTOM'. If 'CUSTOM', define metric in core.ModelEvaluator (mandatory)
  equal_weights_pred_steps: True  # Boolean if error of steps ahead should be weighted evenly for averaging (mandatory)
  error_weights: [ 1, 1, 1, 1 ]  # Weighting of step ahead errors if equal_weights_pred_steps = False (mandatory)
  pruning: False  #  Boolean to (de)activate optuna trial pruning. Not supported by all models (mandatory)
  early_stopping: False  # Boolean to (de)activate early stopping. Not supported by all models (mandatory)
  es_patience: 10  # Num of epochs without improvement before stopping training. Only if early_stopping=True (mandatory)
  retrain_dur_selection: False  # Retrain frequency (every n-th step) during selection. False to deactivate (optional)
  retrain_length_dur_selection:  # Length of moving retrain data window. If False, complete historical data. (optional)

Prediction:
  retrain_dur_prediction: True # Retrain freq (every n-th step) at test & new set prediction. False to deactivate (optional)
  retrain_length_dur_prediction:  # Length of moving retrain data window. If False, complete historical data. (optional)

Anomaly_detection:
  anomaly_window: 1  # Number of predictions to jointly evaluate for declaring anomalies (mandatory)
  threshold: 3  # Anomaly score above which observations are flagged. Highest score during test data=1 (mandatory)
  anomaly_types: True  # Boolean to (de)activate anomaly classification, e.g. positive or negative direction (mandatory)
  Anomaly_ground_truth:  # Description of anomaly ground truth data. If available, included in the plots (optional)
     [{"Type":"Technical failure",
       "Victim":"PV inverter",
       "Start":"2022-09-19 11:00:00",
       "End":"2022-09-19 12:00:00",
       "Reasoning":""},]

Results:
  save_test_set_metrics: True  # Boolean to decide whether to save performance metrics for test predictions (mandatory)
  save_new_set_metrics: False  # Boolean to decide whether to save performance metrics for new predictions (mandatory)
  save_parameter_importances: False  # Boolean whether to save parameter importances from model selection (mandatory)
  save_optimal_hyperparameters: False  # Boolean whether to save set of optimal params from model selection (mandatory)
  save_test_set_predictions: True  # Boolean whether to save predictions of the test set (mandatory)
  save_new_set_predictions: False  # Boolean whether to save predictions of the new set (mandatory)

Plotting:
  save_parameter_importances_plot: False  # Boolean whether to save plot of param importances from selection (mandatory)
  save_optimization_history_plot: False  # Boolean whether to save model selection progress plot (mandatory)
  save_test_set_predictions_plots: True  # Boolean whether to save plot of the test set predictions (mandatory)
  save_new_set_predictions_plots: True  # Boolean whether to save plot of the new set predictions (mandatory)
  save_shap_explainability_plots: True  # Boolean whether to save plot Shap explainability plots (mandatory)
  steps_to_plot: [1,3]  # List of steps ahead to be plotted. If empty or not provided, all steps plotted. (optional)

Computation:
  GPU: False  # Boolean to decide whether to train models on GPU. Not supported by all machines and models (mandatory)
  n_cores: -1  # Number of CPU cores to be used for parallel model training (mandatory)
  selection_on_multiple_machines: False  # Enables running selection on multiple machines via shared sql-db (mandatory)
