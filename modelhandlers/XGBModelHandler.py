from core.ModelHandler import ModelHandler
from optuna.integration import XGBoostPruningCallback
import logging

class XGBModelHandler(ModelHandler):
    """
    A child class of the modelhandler class for the XGBModel model. This class is responsible
    for managing model-specific parameters.

    Args:
        config (dict): Configuration dictionary containing model settings.
        logtime (str): A timestamp or identifier for logging purposes.
    """
    def __init__(self, config, logtime):
        super().__init__(config, logtime)

    def get_general_auto_parameter(self):

        auto_params = \
        {"output_chunk_length": self.mdl_cfg.forecasting_horizon,
        "lags_future_covariates": (0, self.mdl_cfg.forecasting_horizon) if self.mdl_cfg.future_covariates else None,
        "lags_past_covariates": 0 if self.mdl_cfg.past_covariates else None,
        "likelihood": "quantile" if self.mdl_cfg.probabilistic else None,
        "quantiles": self.mdl_cfg.quantiles if self.mdl_cfg.probabilistic else None,
        'device': "gpu" if self.mdl_cfg.GPU else None,
        'n_jobs': self.mdl_cfg.n_cores,
        #'metric': ['mae', 'rmse', 'quantile'],
         }

        return {key: value for key, value in auto_params.items() if value}

    def get_model_selection_auto_parameter(self, trial=None):
        return {}

    def get_model_selection_fitting_params(self, trial=None):
        return {}

    def model_specific_param_transformations(self, hp, trial=None):
        return hp

    def set_max_history_window(self):
        return self.hp['lags']["max"] if isinstance(self.hp['lags'], dict) else self.hp['lags']
