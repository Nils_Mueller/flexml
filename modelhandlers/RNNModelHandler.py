from core.ModelHandler import ModelHandler
from darts.utils.likelihood_models import GaussianLikelihood, QuantileRegression, LaplaceLikelihood
import torch
from core.PyTorchLightningPruningCallback import PyTorchLightningPruningCallback
from pytorch_lightning.callbacks import EarlyStopping
#from optuna.integration import PyTorchLightningPruningCallback

class RNNModelHandler(ModelHandler):
    """
    A child class of the modelhandler class for the RNN model. This class is responsible
    for managing model-specific parameters.

    Args:
        config (dict): Configuration dictionary containing model settings.
        logtime (str): A timestamp or identifier for logging purposes.
    """
    def __init__(self, config, logtime):
        super().__init__(config, logtime)

    def get_general_auto_parameter(self):
        if self.mdl_cfg.probabilistic:
            if self.mdl_cfg.probability_model == "quantile_reg":
                likelihood = QuantileRegression(quantiles=self.mdl_cfg.quantiles)
            elif self.mdl_cfg.probability_model == "Gaussian":
                likelihood = GaussianLikelihood()
            elif self.mdl_cfg.probability_model == "LaPlace":
                likelihood = LaplaceLikelihood()
            else:
                err_msg = f"Probability model {self.mdl_cfg.probability_model} does not exist."
                raise ValueError(err_msg)
        else:
            likelihood = None
        general_auto_params = {"likelihood": likelihood,
       "pl_trainer_kwargs": {"accelerator": "gpu", "devices": -1, "auto_select_gpus": True} if self.mdl_cfg.GPU else {},}
        return {key: value for key, value in general_auto_params.items() if value}

    def get_model_selection_auto_parameter(self, trial=None):
        model_selection_auto_params = {}
        callbacks = []
        if self.mdl_cfg.pruning:
            pruner = PyTorchLightningPruningCallback(trial, monitor="val_loss")
            callbacks = callbacks + [pruner]
        if self.mdl_cfg.early_stopping:
            early_stopper = EarlyStopping("val_loss", patience=self.mdl_cfg.es_patience, verbose=True)
            callbacks = callbacks + [early_stopper]
            model_selection_auto_params["model_name"] = f"{self.logtime}_{self.mdl_cfg.type}"
            model_selection_auto_params["force_reset"] = True
            model_selection_auto_params["save_checkpoints"] = True
        model_selection_auto_params.update({"pl_trainer_kwargs": {"callbacks": callbacks} if callbacks else None})
        return {key: value for key, value in model_selection_auto_params.items() if value}

    def model_specific_param_transformations(self, hp, trial=None):
        if "optimizer_cls" in hp:
            hp["optimizer_cls"] = getattr(torch.optim, hp["optimizer_cls"])
        if "lr" in hp:
            if 'optimizer_kwargs' in hp:
                hp['optimizer_kwargs'].update({"lr": hp["lr"]})
            else:
                hp['optimizer_kwargs'] = {"lr": float(hp["lr"])}
            del hp['lr']
        return hp

    def set_max_history_window(self):
        return self.hp['input_chunk_length']["max"] if isinstance(self.hp['input_chunk_length'], dict) else self.hp['input_chunk_length']
