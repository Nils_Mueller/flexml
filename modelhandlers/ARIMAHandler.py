from core.ModelHandler import ModelHandler

class ARIMAHandler(ModelHandler):
    """
    A child class of the modelhandler class for the ARIMA model. This class is responsible
    for managing model-specific parameters.

    Args:
        config (dict): Configuration dictionary containing model settings.
        logtime (str): A timestamp or identifier for logging purposes.
    """
    def __init__(self, config, logtime):
        super().__init__(config, logtime)

    def get_general_auto_parameter(self):
        return {}

    def get_model_selection_auto_parameter(self, trial=None):
        return {}

    def get_model_selection_fitting_params(self, trial=None):
        return {}

    def model_specific_param_transformations(self, hp, trial=None):
        return hp

    def set_max_history_window(self):
        return self.hp['p']["max"] if isinstance(self.hp['p'], dict) else self.hp['p']

