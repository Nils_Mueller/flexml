from core.ModelHandler import ModelHandler
from optuna.integration import LightGBMPruningCallback
import logging

class LightGBMModelHandler(ModelHandler):
    """
    A child class of the modelhandler class for the LightGBM model. This class is responsible
    for managing model-specific parameters.

    Args:
        config (dict): Configuration dictionary containing model settings.
        logtime (str): A timestamp or identifier for logging purposes.
    """
    def __init__(self, config, logtime):
        super().__init__(config, logtime)

    def get_general_auto_parameter(self):

        auto_params = \
        {"output_chunk_length": self.mdl_cfg.forecasting_horizon,
        "lags_future_covariates": (0, self.mdl_cfg.forecasting_horizon) if self.mdl_cfg.future_covariates else None,
        "lags_past_covariates": 0 if self.mdl_cfg.past_covariates else None,
        "likelihood": "quantile" if self.mdl_cfg.probabilistic else None,
        "quantiles": self.mdl_cfg.quantiles if self.mdl_cfg.probabilistic else None,
        'device': "gpu" if self.mdl_cfg.GPU else None,
        'n_jobs': self.mdl_cfg.n_cores,
        'metric': ['mae', 'rmse', 'quantile'],

         }

        return {key: value for key, value in auto_params.items() if value}

    def get_model_selection_auto_parameter(self, trial=None):
        return {}

    def get_model_selection_fitting_params(self, trial=None):
        if self.mdl_cfg.error_metric == "RMSE":
            val_metric = "rmse"
        elif self.mdl_cfg.error_metric == "MAE":
            val_metric = "mae"
        elif self.mdl_cfg.error_metric == "PINBALL":
            val_metric = "quantile"
        elif self.mdl_cfg.error_metric == "CUSTOM":
            val_metric = "rmse"
            logging.warning("Currently no custom metric for pruning enabled. Using RMSE for trial pruning.")
        else:
            err_msg = f"Error metric {self.mdl_cfg.error_metric} not implemented."
            raise ValueError(err_msg)
        callbacks = []
        if self.mdl_cfg.pruning:
            pruner = LightGBMPruningCallback(trial, val_metric)
            callbacks = callbacks + [pruner]
        model_selection_auto_params = {"callbacks": callbacks if callbacks else None}
        return {key: value for key, value in model_selection_auto_params.items() if value}

    def model_specific_param_transformations(self, hp, trial=None):
        return hp

    def set_max_history_window(self):
        return self.hp['lags']["max"] if isinstance(self.hp['lags'], dict) else self.hp['lags']
