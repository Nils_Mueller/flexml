from typing import Union, Literal
from pydantic import BaseModel
from core.util import flatten_dict

class ModelEvaluatorConfig(BaseModel):
    """
    Configuration class for model evaluation. Defines the parameters used in the evaluation process,
    including error metrics, prediction horizons, quantiles, and error weights.

    Attributes:
        error_metric (Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]): Error metric used.
        predict_full_horizon (bool): Flag indicating that all steps predicted.
        forecasting_horizon (int): The number of steps ahead predicted.
        quantiles (Union[list, None]): List of quantiles for evaluation.
        equal_weights_pred_steps (bool): Whether to apply equal weights across prediction steps.
        error_weights (list): List of weights for each prediction step's error in evaluation.

    Methods:
    from_config(config): Creates an instance of ModelEvaluatorConfig from a configuration object.
    from_dict(config): Creates an instance of ModelEvaluatorConfig from a dictionary.
    """
    error_metric: Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]
    predict_full_horizon: bool
    forecasting_horizon: int
    quantiles: Union[list, None]
    equal_weights_pred_steps: bool
    error_weights: list

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of ModelEvaluatorConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for model evaluation.

        Returns:
            ModelEvaluatorConfig: An instance of the ModelEvaluatorConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of ModelEvaluatorConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for model evaluation.

        Returns:
            ModelEvaluatorConfig: An instance of the ModelEvaluatorConfig class.
        """
        return cls(**config)
