from typing import Union, List, Dict
from pydantic import BaseModel, field_validator, model_validator, conint
from core.util import flatten_dict
from core.ConfigValidator import validate_scaling_method, check_model_type_availability, \
    check_if_tscv_if_early_stopping_or_pruning, check_early_stopping_capability, \
    check_pruning_capability


class ModelSelectorConfig(BaseModel):
    """
    Configuration class for model selection, including cross-validation settings,
    early stopping, pruning, and hyperparameter optimization.

    Attributes:
        CV_splits (int): Number of cross-validation splits to use.
        type (str): The type of model.
        Name (str): Name of the experiment.
        selection_on_multiple_machines (bool): Whether to distribute selection across machines.
        n_trials (int): Number of trials for optimization.
        timeout (int): Timeout for the model selection process.
        early_stopping (bool): Flag indicating whether early stopping is enabled.
        es_patience (conint(ge=1)): Patience parameter for early stopping.
        pruning (bool): Flag indicating whether pruning is enabled.
        save_parameter_importances (bool): Flag to save parameter importances.
        save_parameter_importances_plot (bool): Flag to save parameter importances plot.
        save_optimization_history_plot (bool): Flag to save the optimization history plot.
        save_optimal_hyperparameters (bool): Flag to save the optimal hyperparameters.
        scaling_method: Method for scaling features.

    Methods:
        from_config(config): Creates an instance of ModelSelectorConfig from a configuration object.
        from_dict(config): Creates an instance of ModelSelectorConfig from a dictionary.
    """
    CV_splits: int
    type: str
    Name: str
    selection_on_multiple_machines: bool
    n_trials: int
    timeout: int
    early_stopping: bool
    es_patience: conint(ge=1)
    pruning: bool
    save_parameter_importances: bool
    save_parameter_importances_plot: bool
    save_optimization_history_plot: bool
    save_optimal_hyperparameters: bool
    scaling_method: Union[str, Dict[str, Union[str, List[str]]]]

    @field_validator("type")
    def check_model_type_availability(cls, v):
        """
        Validates the model type to ensure it is available.

        Args:
            v (str): The model type value to be validated.

        Returns:
            str: The validated model type.

        Raises:
            ValueError: If the model type is not available.
        """
        return check_model_type_availability(v)

    @field_validator("scaling_method")
    def validate_scaling_method(cls, v):
        """
        Validates the scaling method to ensure it is valid.

        Args:
            v (Union[str, Dict[str, Union[str, List[str]]]]): The scaling method.

        Returns:
            Union[str, Dict[str, Union[str, List[str]]]]: The validated scaling method.

        Raises:
            ValueError: If the scaling method is invalid.
        """
        return validate_scaling_method(v)

    @model_validator(mode="after")
    def check_cv_if_early_stopping_or_pruning(self):
        """
        Ensures that time series cross-validation (TSCV) is not used if
        early stopping or pruning is enabled.

        This method checks that `early_stopping` or `pruning` flags are
        set to False if CV_splits > 1, i.e. time series cross-validation.

        Returns:
            ModelSelectorConfig: The validated model selection configuration.

        Raises:
            ValueError: If early stopping or pruning is enabled although TSCV activated.
        """
        return check_if_tscv_if_early_stopping_or_pruning(self)

    @model_validator(mode="after")
    def check_early_stopping_capability(self):
        """
         Validates that the model supports early stopping if the `early_stopping` flag is enabled.

         This method ensures that the model is capable of early stopping, checking compatibility
         based on the model type and the `early_stopping` configuration.

         Returns:
             ModelSelectorConfig: The validated model selection configuration.

         Raises:
             ValueError: If early stopping is enabled but the model does not support it.
         """
        return check_early_stopping_capability(self)

    @model_validator(mode="after")
    def check_pruning_capability(self):
        """
         Validates that the model supports pruning if the `pruning` flag is enabled.

         This method ensures that pruning is supported by the selected model, and if not,
         raises an error if the `pruning` flag is set to True.

         Returns:
             ModelSelectorConfig: The validated model selection configuration.

         Raises:
             ValueError: If pruning is enabled but the model does not support it.
         """
        return check_pruning_capability(self)

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of ModelSelectorConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for model selection.

        Returns:
            ModelSelectorConfig: An instance of the ModelSelectorConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of ModelSelectorConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for model selection.

        Returns:
            ModelSelectorConfig: An instance of the ModelSelectorConfig class.
        """
        return cls(**config)
