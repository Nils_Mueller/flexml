from typing import Union
from pydantic import BaseModel
from core.util import flatten_dict

class AnomalyDetectorConfig(BaseModel):
    """
    Configuration class for the Anomaly Detector. Defines the parameters required for
    anomaly detection, including quantiles, anomaly window size, threshold, and anomaly types.

    Attributes:
        quantiles (Union[list, None]): List of quantiles used for anomaly detection or None.
        anomaly_window (int): The size of the window used to detect anomalies.
        threshold (Union[float, None]): The threshold value for anomaly detection or None.
        anomaly_types (bool): Flag indicating the types of anomalies to detect.

    Methods:
    from_config(config): Creates an instance of AnomalyDetectorConfig from a configuration object.
    from_dict(config): Creates an instance of AnomalyDetectorConfig from a dictionary.
    """
    quantiles: Union[list, None]
    anomaly_window: int
    threshold: Union[float, None]
    anomaly_types: bool

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of AnomalyDetectorConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for anomaly detection.

        Returns:
            AnomalyDetectorConfig: An instance of the AnomalyDetectorConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of AnomalyDetectorConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for anomaly detection.

        Returns:
            AnomalyDetectorConfig: An instance of the AnomalyDetectorConfig class.
        """
        return cls(**config)
