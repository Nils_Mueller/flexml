from typing import List, Dict, Literal, Union
from pydantic import BaseModel, field_validator, model_validator, conint
from core.util import flatten_dict
from core.ConfigValidator import validate_scaling_method, check_model_type_availability, \
    check_num_samples_if_probabilistic, check_if_model_supports_probabilistic_predictions, \
    check_if_none_false_or_pos_int, check_error_metric


class ModelHandlerConfig(BaseModel):
    """
    Configuration class for model handling, including model type, forecasting horizon,
    probabilistic settings, scaling methods, retraining parameters, and evaluation metrics.

    Attributes:
        type (str): The type of model being used.
        forecasting_horizon (int): The number of time steps to forecast ahead.
        predict_full_horizon (bool): Whether to predict all steps ahead.
        probabilistic (bool): Flag indicating whether the model is probabilistic.
        num_samples (Union[int, None]): The number of samples to use for probabilistic models.
        probability_model (Union[str, None]): The probability model to use.
        quantiles (Union[list, None]): List of quantiles for probabilistic predictions.
        hyperparameters (Union[dict, None]): Hyperparameters for the model.
        scaling_method (Union[str, Dict[str, Union[str, List[str]]]]): The method for scaling.
        future_covariates (Union[str, List[str], None]): The features to use as future covariates.
        past_covariates (Union[str, List[str], None]): The features to use as past covariates.
        pruning (bool): Flag indicating whether pruning is enabled.
        early_stopping (bool): Flag indicating whether early stopping is enabled.
        retrain_dur_prediction: If and after how many steps to retrain during prediction.
        retrain_length_dur_prediction: Moving history window for retraining during prediction.
        retrain_dur_selection: If and after how many steps to retrain during selection.
        retrain_length_dur_selection: Moving history window for retraining during selection.
        GPU (bool): Flag indicating whether to use GPU for model training.
        n_cores (int): The number of CPU cores to use for training.
        error_metric (Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]): Error metric for evaluation.

    Methods:
    from_config(config): Creates an instance of ModelHandlerConfig from a configuration object.
    from_dict(config): Creates an instance of ModelHandlerConfig from a dictionary.
    """
    type: str
    forecasting_horizon: int
    predict_full_horizon: bool
    probabilistic: bool
    num_samples: Union[int, None]
    probability_model: Union[str, None]
    quantiles: Union[list, None]
    hyperparameters: Union[dict, None]
    scaling_method: Union[str, Dict[str, Union[str, List[str]]]]
    future_covariates: Union[str, List[str], None]
    past_covariates: Union[str, List[str], None]
    pruning: bool
    early_stopping: bool
    retrain_dur_prediction: Union[conint(ge=1), bool, None]
    retrain_length_dur_prediction: Union[conint(ge=1), bool, None]
    retrain_dur_selection: Union[conint(ge=1), bool, None]
    retrain_length_dur_selection: Union[conint(ge=1), bool, None]
    GPU: bool
    n_cores: int
    error_metric: Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]

    @model_validator(mode="after")
    def default_params_if_provided_as_none(self):
        """
        Sets default values for parameters provided as None after model initialization.

        This method ensures that certain parameters are set to default values if not
        explicitly provided. Specifically, it sets `hyperparameters` to an empty
        dictionary and retraining to `False` if they are `None`.

        Returns:
            ModelHandlerConfig: The updated model configuration with default values applied.
        """
        self.hyperparameters = self.hyperparameters or {}
        self.retrain_dur_selection = self.retrain_dur_selection or False
        self.retrain_dur_prediction = self.retrain_dur_prediction or False
        return self

    @model_validator(mode="after")
    def check_if_model_supports_probabilistic_predictions(self):
        """
        Validates if the model supports probabilistic predictions.

        This method checks whether the configured model is capable of performing
        probabilistic predictions. It ensures that the model is set up correctly
        for probabilistic forecasting if the `probabilistic` flag is set to True.

        Returns:
            ModelHandlerConfig: The validated model configuration.

        Raises:
            ValueError: If the model does not support probabilistic predictions
                        while the `probabilistic` flag is set to True.
        """
        return check_if_model_supports_probabilistic_predictions(self)

    @field_validator("type")
    def check_model_type_availability(cls, v):
        """
        Validates the model type to ensure it is available.

        Args:
            v (str): The model type value to be validated.

        Returns:
            str: The validated model type.

        Raises:
            ValueError: If the model type is not available.
        """
        return check_model_type_availability(v)

    @field_validator("scaling_method")
    def validate_scaling_method(cls, v):
        """
        Validates the scaling method to ensure it is vali.

        Args:
            v (Union[str, Dict[str, Union[str, List[str]]]]): The scaling method or configuration.

        Returns:
            Union[str, Dict[str, Union[str, List[str]]]]: The validated scaling method.

        Raises:
            ValueError: If the scaling method is invalid.
        """
        return validate_scaling_method(v)

    @model_validator(mode="after")
    def check_num_samples_if_probabilistic(self):
        """
        Checks if the number of samples is provided for probabilistic models.

        This method ensures that if the model is probabilistic, the number
        of samples (`num_samples`) is properly set. If `probabilistic` is
        `True`, `num_samples` should be an integer.

        Returns:
            ModelHandlerConfig: The validated model configuration.
        """
        return check_num_samples_if_probabilistic(self)

    @field_validator('retrain_length_dur_prediction')
    def validate_retrain_length_dur_prediction(cls, v):
        """
        Validates the retraining settings during prediction.

        Ensures that the value is either `None`, `False`, or a positive integer.

        Args:
            v (Union[bool, conint(ge=1), None]): The retrain frequency value.

        Returns:
            Union[bool, conint(ge=1), None]: The validated retrain frequency.
        """
        return check_if_none_false_or_pos_int(v)

    @field_validator('retrain_length_dur_selection')
    def validate_retrain_length_dur_selection(cls, v):
        """
        Validates the retraining settings during selection.

        Ensures that the value is either `None`, `False`, or a positive integer.

        Args:
            v (Union[bool, conint(ge=1), None]): The retrain frequency.

        Returns:
            Union[bool, conint(ge=1), None]: The validated retrain frequency.
        """
        return check_if_none_false_or_pos_int(v)

    @model_validator(mode="after")
    def check_if_pinball_selected_for_deterministic_models(self):
        """
        Validates that the PINBALL error metric is not selected for deterministic models.

        This method ensures that if the model is deterministic
        (i.e., the `probabilistic` flag is False), the `error_metric` is not
        set to "PINBALL". The PINBALL error metric is only applicable to
        probabilistic models.

        Returns:
            ModelHandlerConfig: The validated model configuration.

        Raises:
            ValueError: If the `error_metric` is set to "PINBALL" while the model is deterministic.
        """
        return check_error_metric(self)

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of ModelHandlerConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for model handling.

        Returns:
            ModelHandlerConfig: An instance of the ModelHandlerConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of ModelHandlerConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for model handling.

        Returns:
            ModelHandlerConfig: An instance of the ModelHandlerConfig class.
        """
        return cls(**config)
