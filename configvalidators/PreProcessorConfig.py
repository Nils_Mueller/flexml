from typing import Union, List, Dict
from pydantic import BaseModel, field_validator, Field
from core.util import flatten_dict
from core.ConfigValidator import validate_scaling_method

class PreProcessorConfig(BaseModel):
    """
    Configuration class for preprocessing operations in the pipeline, defining settings
    for interpolation, resampling, scaling, and other data processing tasks.

    Attributes:
        interpolate (bool): Flag to determine if interpolation should be applied to missing data.
        resample (bool): Flag to determine if resampling should be performed.
        resampling_rule (str): The rule or method to use for resampling the data.
        difference_target (bool): Flag if the target feature should be differenced.
        difference_future_covariates (bool): Flag if future covariates should be differenced.
        difference_past_covariates (bool): Flag if past covariates should be differenced.
        scaling_method (Union[str, Dict[str, Union[str, List[str]]]]): The scaling method to apply.
        scale_y (bool): Flag to determine if the target feature (y) should be scaled.
        hist_data_training_size (float): Fraction of historical data used for training/selection.
        target_feature (str): The target feature to predict.
        future_covariates (Union[str, List[str], None]): Future covariates to use for the model.
        past_covariates (Union[str, List[str], None]): Past covariates to use for the model.
        train_size (float): Fraction of historical training/validation data used for training.

    Methods:
        from_config(config): Creates instance of PreProcessorConfig from given configuration object.
        from_dict(config): Creates instance of PreProcessorConfig from a dictionary.
    """
    interpolate: bool
    resample: bool
    resampling_rule: str
    difference_target: bool
    difference_future_covariates: bool
    difference_past_covariates: bool
    scaling_method: Union[str, Dict[str, Union[str, List[str]]]]
    scale_y: bool
    hist_data_training_size: float = Field(gt=0, lt=1)
    target_feature: str
    future_covariates: Union[str, List[str], None]
    past_covariates: Union[str, List[str], None]
    train_size: float = Field(gt=0, lt=1)

    @field_validator("scaling_method")
    def validate_scaling_method(cls, v):
        """
        Validates the scaling method.

        Args:
            v (Union[str, Dict[str, Union[str, List[str]]]]): The scaling method.

        Returns:
            Union[str, Dict[str, Union[str, List[str]]]]: The validated scaling method.

        Raises:
            ValueError: If the scaling method is invalid.
        """
        return validate_scaling_method(v)

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of PreProcessorConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for pre-processing.

        Returns:
            PreProcessorConfig: An instance of the PreProcessorConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of PreProcessorConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for pre-processing.

        Returns:
            PreProcessorConfig: An instance of the PreProcessorConfig class.
        """
        return cls(**config)
