from pydantic import BaseModel
from core.util import flatten_dict

class ResultStorerConfig(BaseModel):
    """
    Configuration class for storing results related to model evaluations and predictions.

    Attributes:
        save_test_set_metrics (bool): Flag to determine if test set metrics should be saved.
        save_new_set_metrics (bool): Flag to determine if new set metrics should be saved.
        save_parameter_importances (bool): If parameter importances should be saved.
        save_optimal_hyperparameters (bool): If optimal hyperparameters should be saved.
        save_test_set_predictions (bool): If test set predictions should be saved.
        save_new_set_predictions (bool): If new set predictions should be saved.
        forecasting_horizon (int): The number of steps ahead for which forecasting is done.
        predict_full_horizon (bool): If the full forecasting horizon should be predicted.
        Name (str): Name of the experiment.
        type (str): The model type.

    Methods:
        from_config(config): Creates instance of ResultStorerConfig from given configuration object.
        from_dict(config): Creates an instance of ResultStorerConfig from a dictionary.
    """
    save_test_set_metrics: bool
    save_new_set_metrics: bool
    save_parameter_importances: bool
    save_optimal_hyperparameters: bool
    save_test_set_predictions: bool
    save_new_set_predictions: bool
    forecasting_horizon: int
    predict_full_horizon: bool
    Name: str
    type: str

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of ResultStorerConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for storing results.

        Returns:
            ResultStorerConfig: An instance of the ResultStorerConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of ResultStorerConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for storing results.

        Returns:
            ResultStorerConfig: An instance of the ResultStorerConfig class.
        """
        return cls(**config)
