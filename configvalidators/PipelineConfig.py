from pydantic import BaseModel, field_validator, model_validator
from typing import Union, List, Dict, Literal
from core.util import flatten_dict
from core.ConfigValidator import validate_scaling_method, check_model_type_availability, \
    check_params_for_model_selection_status, check_for_shap_support, \
    check_probabilistic_for_anomaly_detection


class PipelineConfig(BaseModel):
    """
    Configuration class for the pipeline, including settings for model selection,
    anomaly detection, scaling, hyperparameters, and saving predictions and metrics.

    Attributes:
        model_selection (bool): Flag indicating if model selection is enabled.
        predict_test_data (bool): Flag indicating if predictions for test data should be generated.
        predict_new_data (bool): Flag indicating if predictions for new data should be generated.
        anomaly_detection (bool): Flag indicating if anomaly detection is enabled.
        type (str): The type of model to use.
        probabilistic (bool): Flag indicating whether the model is probabilistic.
        scaling_method (Union[str, Dict[str, Union[str, List[str]]]]): Method to scale the features.
        hyperparameters (Union[dict, None]): Hyperparameters for the model.
        error_metric (Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]): The error metric.
        save_test_set_predictions (bool): Flag to save predictions for the test set.
        save_new_set_predictions (bool): Flag to save predictions for the new set.
        save_test_set_metrics (bool): Flag to save metrics for the test set.
        save_new_set_metrics (bool): Flag to save metrics for the new set.
        save_test_set_predictions_plots (bool): Flag to save plots of the test set predictions.
        save_new_set_predictions_plots (bool): Flag to save plots of the new set predictions.
        save_shap_explainability_plots (bool): Flag to save SHAP explainability plots.

    Methods:
        from_config(config): Creates an instance of PipelineConfig from a configuration object.
        from_dict(config): Creates an instance of PipelineConfig from a dictionary.
    """
    model_selection: bool
    predict_test_data: bool
    predict_new_data: bool
    anomaly_detection: bool
    type: str
    probabilistic: bool
    scaling_method: Union[str, Dict[str, Union[str, List[str]]]]
    hyperparameters: Union[dict, None]
    error_metric: Literal["RMSE", "MAE", "PINBALL", "CUSTOM"]
    save_test_set_predictions: bool
    save_new_set_predictions: bool
    save_test_set_metrics: bool
    save_new_set_metrics: bool
    save_test_set_predictions_plots: bool  # If test set prediction should be plotted
    save_new_set_predictions_plots: bool
    save_shap_explainability_plots: bool

    @field_validator("type")
    def check_model_type_availability(cls, v):
        """
        Validates the model type to ensure it is available.

        Args:
            v (str): The model type value to be validated.

        Returns:
            str: The validated model type.

        Raises:
            ValueError: If the model type is not available.
        """
        return check_model_type_availability(v)

    @field_validator("scaling_method")
    def validate_scaling_method(cls, v):
        """
        Validates the scaling method.

        Args:
            v (Union[str, Dict[str, Union[str, List[str]]]]): The scaling method.

        Returns:
            Union[str, Dict[str, Union[str, List[str]]]]: The validated scaling method.

        Raises:
            ValueError: If the scaling method is invalid.
        """
        return validate_scaling_method(v)

    @model_validator(mode='after')
    def check_params_for_model_selection_status(self):
        """
        Validates if hyperparameters are configured correctly for selected model selection status.

        This method ensures that hyperparameter search spaces are only defined if model
         selection is activated.

        Returns:
            PipelineConfig: The validated pipeline configuration.

        Raises:
            ValueError: If the hyperparameters do not align with model selection status.
        """
        return check_params_for_model_selection_status(self)

    @model_validator(mode='after')
    def check_for_shap_support(self):
        """
         Validates whether the SHAP (SHapley Additive exPlanations) explainability plots
         are supported by the model.

         This method ensures that SHAP plots are only activated if the model supports it.

         Returns:
             PipelineConfig: The validated pipeline configuration.

         Raises:
             ValueError: If SHAP explainability plots are requested but not supported.
         """
        return check_for_shap_support(self)

    @model_validator(mode='after')
    def check_probabilistic_for_anomaly_detection(self):
        """
        Ensures that a probabilistic model is used for anomaly detection.

        This method verifies that if anomaly detection is enabled, the model
        must be probabilistic.

        Returns:
            PipelineConfig: The validated pipeline configuration.

        Raises:
            ValueError: If anomaly detection is enabled without a probabilistic model.
        """
        return check_probabilistic_for_anomaly_detection(self)

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of PipelineConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for the pipeline.

        Returns:
            PipelineConfig: An instance of the PipelineConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of PipelineConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for the pipeline.

        Returns:
            PipelineConfig: An instance of the PipelineConfig class.
        """
        return cls(**config)
