from typing import Union, List, Dict
from pydantic import BaseModel, field_validator, model_validator
from core.util import flatten_dict
from core.ConfigValidator import validate_anomaly_ground_truth, check_steps_to_plot_feasibility

class PlotterConfig(BaseModel):
    """
    Configuration class for the plotter component in the pipeline, defining settings
    for plotting steps, anomaly detection, and other visualization configurations.

    Attributes:
        steps_to_plot (Union[list, None]): List of steps for which to generate plots.
        type (str): The type of model to use.
        forecasting_horizon (int): The number of steps forecasted ahead.
        predict_full_horizon (bool): Flag if all steps ahead are predicted.
        threshold (Union[float, None]): The threshold for anomaly detection.
        anomaly_detection (bool): Flag to indicate whether anomaly detection was enabled.
        quantiles (Union[list, None]): List of predicted quantiles.
        target_feature (str): The predicted target feature.
        probabilistic (bool): Flag indicating whether the model was probabilistic.
        Name (str): The name of the experiment.
        Anomaly_ground_truth: Ground truth labels for anomaly detection.

    Methods:
        from_config(config): Creates an instance of PlotterConfig from a given configuration object.
        from_dict(config): Creates an instance of PlotterConfig from a dictionary.
    """
    steps_to_plot: Union[list, None]
    type: str
    forecasting_horizon: int
    threshold: Union[float, None]
    anomaly_detection: bool
    quantiles: Union[list, None]
    target_feature: str
    probabilistic: bool
    Name: str
    Anomaly_ground_truth: Union[List[Dict[str, str]], None]
    predict_full_horizon: bool

    @field_validator("Anomaly_ground_truth")
    def validate_anomaly_ground_truth(cls, v):
        """
        Validates the anomaly ground truth labels to ensure they are correctly formatted.

        Args:
            v (Union[List[Dict[str, str]], None]): The anomaly ground truth labels.

        Returns:
            Union[List[Dict[str, str]], None]: The validated anomaly ground truth labels.

        Raises:
            ValueError: If the format of the anomaly ground truth labels is incorrect.
        """
        return validate_anomaly_ground_truth(v)

    @model_validator(mode='after')
    def check_steps_to_plot_feasibility(self):
        """
        Validates whether plotting the requested steps is feasible based on the forecasting horizon.

        This method ensures that the `steps_to_plot` is correctly set according to the
        forecasting horizon and other configuration parameters.

        Returns:
            PlotterConfig: The validated plotter configuration with updated plotting steps.

        Raises:
            ValueError: If the steps to plot exceed the available forecasting horizon.
        """
        return check_steps_to_plot_feasibility(self)

    @model_validator(mode="after")
    def default_params(self):
        """
        Sets default parameters for `steps_to_plot` if not provided.

        If `steps_to_plot` is empty or None, it will be populated based on the `forecasting_horizon`
        and whether `predict_full_horizon` is enabled.

        Returns:
            PlotterConfig: The updated plotter configuration with default parameters set.
        """
        if self.steps_to_plot in [None, []]:
            if self.predict_full_horizon:
                self.steps_to_plot = [i for i in range(1, self.forecasting_horizon+1)]
            else:
                self.steps_to_plot = [self.forecasting_horizon]
        return self

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of PlotterConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for plotting.

        Returns:
            PlotterConfig: An instance of the PlotterConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of PlotterConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for plotting.

        Returns:
            PlotterConfig: An instance of the PlotterConfig class.
        """
        return cls(**config)
