from typing import Union, List
from pydantic import BaseModel, field_validator
from core.util import flatten_dict
from core.ConfigValidator import validate_hist_data_filename, validate_new_data_filename, \
    check_if_none_false_or_between_0_1

class DataLoaderConfig(BaseModel):
    """
    Configuration class for the data loader. Defines the parameters required
    for loading and processing data, including historical and new data filenames,
    target features, and covariates.

    Attributes:
        hist_data_filename (str): The filename of the historical data.
        new_data_filename (Union[str, None]): The filename of the new data.
        try_out_dataset (Union[float, bool, None]): Fraction of the dataset to be used.
        target_feature (str): The name of the target feature in the dataset.
        future_covariates (Union[str, List[str], None]): Features to be used as future covariates.
        past_covariates (Union[str, List[str], None]): Features to be used as past covariates.

    Methods:
    from_config(config): Creates an instance of DataLoaderConfig from a configuration object.
    from_dict(config): Creates an instance of DataLoaderConfig from a dictionary.
    """
    hist_data_filename: str
    new_data_filename: Union[str, None]
    try_out_dataset: Union[float, bool, None]
    target_feature: str
    future_covariates: Union[str, List[str], None]
    past_covariates: Union[str, List[str], None]

    @field_validator('hist_data_filename')
    def validate_hist_data_filename(cls, v):
        """
        Validates the 'hist_data_filename' field to ensure it adheres to the required format.

        Args:
            v (str): The value of the 'hist_data_filename' field.

        Returns:
            str: The validated value of 'hist_data_filename'.

        Raises:
            ValueError: If the filename is not valid.
        """
        return validate_hist_data_filename(v)

    @field_validator('new_data_filename')
    def validate_new_data_filename(cls, v):
        """
        Validates the 'new_data_filename' field to ensure it adheres to the required format.

        Args:
            v (str): The value of the 'new_data_filename' field.

        Returns:
            str: The validated value of 'new_data_filename'.

        Raises:
            ValueError: If the filename is not valid.
        """
        return validate_new_data_filename(v)

    @field_validator('try_out_dataset')
    def validate_try_out_dataset(cls, v):
        """
          Validates the 'try_out_dataset' field to ensure it is either None, False, or a float
          between 0 and 1.

          Args:
              v (Union[float, bool, None]): The value of the 'try_out_dataset' field.

          Returns:
              Union[float, bool, None]: The validated value of 'try_out_dataset'.

          Raises:
              ValueError: If the value is not in the valid range.
          """
        return check_if_none_false_or_between_0_1(v)

    @classmethod
    def from_config(cls, config):
        """
        Creates an instance of DataLoaderConfig from a given configuration object.

        This method assumes the input configuration is a flattened dictionary format,
        and uses the `flatten_dict` function to prepare the configuration for the class
        instantiation.

        Args:
            config (BaseModel): A configuration dictionary containing the necessary settings
                           for data loading.

        Returns:
            DataLoaderConfig: An instance of the DataLoaderConfig class.
        """
        return cls(**flatten_dict(config.model_dump()))

    @classmethod
    def from_dict(cls, config):
        """
        Creates an instance of DataLoaderConfig from a dictionary.

        Args:
            config (dict): A configuration dictionary containing the necessary settings
                           for data loading.

        Returns:
            DataLoaderConfig: An instance of the DataLoaderConfig class.
        """
        return cls(**config)
