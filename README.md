# FlexML - A Flexible and Configurable Time Series Forecasting and Anomaly Detection Pipeline
This repository provides a flexible and highly configurable framework for time series 
forecasting and anomaly detection.
It streamlines the entire workflow by automating essential tasks such as data loading, 
preprocessing, model selection, training (and re-training), prediction, anomaly detection, 
model evaluation, interpretation, and plotting. Designed with flexibility at
its core, this pipeline can be easily adapted to a wide array of use cases, making it a 
versatile solution for time series analysis.

## Table of Contents
- [Overview and Key Features](#overview-and-key-features)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Directory Structure](#directory-structure)
- [Examples](#examples)
- [Contributing](#contributing)
- [License](#license)

## Overview and Key Features
FlexML enables the training, selection, evaluation, and interpretation of forecasting and
anomaly detection models on historical datasets. Additionally, it supports the prediction
of new datasets using models that have been developed based on historical data. The pipeline 
simulates real-world applications by employing rolling forecast techniques, ensuring a robust
and realistic approach to time series forecasting.

#### Simple Use and Configuration
FlexML allows experiments to be defined using a simple YAML configuration file. 
No coding required—users can specify datasets, model types, processing steps, 
and desired results, and FlexML will manage the rest.

#### Variety of Model Types
FlexML supports a range of models, from naive baselines such as persistence forecasts to
advanced statistical, machine learning and deep learning models, including ARIMA, LightGBM, and LSTMs. 

#### Probabilistic Predictions
FlexML offers support for probabilistic modeling techniques, including quantile regression, 
enabling the generation of uncertainty estimates alongside point predictions.

#### Multi-Step Forecasting
FlexML can generate predictions for both single and multiple steps ahead.

#### Covariate Support
FlexML enables the prediction of future values using both past observations of the target feature itself 
and external covariates describing the past and now (e.g., temperature measurements) or future (e.g., time of day), 
allowing additional context to be incorporated into the predictions.

#### Bayesian Hyperparameter Optimization
FlexML utilizes Optuna for Bayesian hyperparameter optimization, facilitating efficient model 
selection while eliminating the need for manual experimentation.

#### Distributed Model Selection
For extensive model selection, FlexML supports running hyperparameter optimization across multiple machines 
in parallel. Results are aggregated into a SQL database, and the optimal parameters are returned automatically.

#### Model Interpretability and Explanation
FlexML includes tools for understanding model behavior, such as the evaluation of hyperparameter importance
and SHAP (SHapley Additive exPlanations) values, which provide insights into model decisions and structure.

#### Automated Evaluation
FlexML automatically computes various performance metrics for models, including both individual and average
performance across multiple forecast steps, simplifying the evaluation process.

#### Custom Performance Metrics
FlexML allows for the definition of custom performance metrics, enabling the tailoring of model evaluation
to specific needs and use cases.

#### Powerful Plotting Support
FlexML offers robust plotting capabilities for visualizing model selection processes, predictions, anomaly 
flags, and SHAP-based model interpretability, aiding in the analysis and presentation of results.

#### Integration and Modularity
FlexML is highly modular, with individual components such as the DataLoader or AnomalyDetector available for integration
into other projects and pipelines. It can be run as a standalone tool or incorporated into larger systems as needed.

## Installation
FlexML is compatible with Python 3.11 and has been tested to ensure functionality with this version.
The following steps can be used for installation:

1. Clone the repository:
```bash
# In Bash terminal (macOS/Linux) or e.g. Anaconda Prompt (Windows)
 git clone https://gitlab.com/Nils_Mueller/flexml.git
 cd flexml
```
2. Create a virtual environment (recommended)
```bash
# macOS/Linux
 python3.11 -m venv venv  
 source venv/bin/activate
 
# Windows
 conda create --name myenv python=3.11
 conda activate myenv
```
3. Install dependencies:
```bash
 # From requirements.txt
 pip install -r requirements.txt
 
 # or individual 
 pip install u8darts[all]==0.32.0  # Relatively heavy dependencies. Can be installed with selected models e.g. pip install u8darts[torch]
 pip install optuna==4.1.0
 pip install optuna-integration==4.2.0
 pip install pydantic==2.10.5
 pip install PyYAML==6.0.2
 ```

## Usage
To run FlexML for a specific experiment configuration, use the following command in the terminal:
```bash
python main.py -c <config_file.yaml>
```

## Configuration

The configuration file is used to define various settings and parameters for running an experiment with FlexML. 
It allows users to customize the experiment setup, preprocessing, model selection, prediction, anomaly detection, 
and results plotting, among other things. The file is structured in YAML format and can be easily edited to suit 
different use cases.

### Example Configuration and Description of Settings
Configuration files are organized into several sections, each with its own purpose. Below is an example of
a configuration file used to run an experiment with FlexML together with explanations of
the different possible settings:

```yaml
Name: example  # Name of the experiment (will be included in result file names) (mandatory)
Comment: This is a config file for exemplifying the use of flexml  # A comment to describe the experiment (mandatory)

Pipeline:
  model_selection: True  # Boolean to (de)activate model selection (mandatory)
  predict_test_data: True  # Boolean to (de)activate test set prediction (mandatory)
  predict_new_data: True  # Boolean to (de)activate new set prediction (mandatory)
  anomaly_detection: True  # Boolean to (de)activate anomaly detection (mandatory)

Datasets:
  hist_data_filename: "historical_data_example.csv"  # Filename of the historical data set in csv format (mandatory)
  new_data_filename: "new_data_example.csv"  # Filename of the new data set in csv format  (optional)
  try_out_dataset: False  # Fraction (0-1) of historical and/or new data to be used. Can also be set False (optional)
  hist_data_training_size: 0.5  # Fraction (0-1) of historical data used for model selection/(re)training (mandatory)
  target_feature: 'T (degC)'  # Target feature to be predicted. Must be available in historical and new data (mandatory)
  future_covariates:  # Covariates known for the future such as time of day. Must be a list or None (optional)
  past_covariates:  # Covariates only known for the past, e.g. power measurements. Must be a list or None (optional)

Preprocessing:
  interpolate: True  # Boolean to (de)activate linear interpolation of missing data (mandatory)
  resample: False  # Boolean to (de)activate resampling of the data (mandatory)
  resampling_rule: "1min"  # Frequency for resampling data. Check Pandas Doc for possible frequencies (mandatory)
  difference_target: False  # Boolean to (de)activate differencing the target feature (mandatory)
  difference_future_covariates: False  # Boolean to (de)activate differencing the future covariates (mandatory)
  difference_past_covariates: False  # Boolean to (de)activate differencing the past covariates (mandatory)
  scaling_method: "minmax"  # minmax, standard, no_scaling or as space {choices:['minmax'],type:categorical} (mandatory)
  scale_y: False  # Boolean to (de)activate scaling of the target feature (mandatory)

Model:
  type: LightGBMModel  # Model type. Can be any of the ones with existing handler in the modelhandlers folder (mandatory)
  forecasting_horizon: 10  # How many steps to be predicted into the future (mandatory)
  predict_full_horizon: True  # Boolean to decide if all steps or only the furthest should be predicted (mandatory)
  probabilistic: True  # Boolean to (de)activate probabilistic predictions. Not supported by all models. (mandatory)
  num_samples: 2  # Number of samples to be drawn if probabilistic predictions. Must be >1 if probabilistic (optional)
  probability_model: "quantile_reg"  # Probability model to be used. "quantile_reg" "gaussian" or "laplace" (optional)
  quantiles: [ 0.1, 0.5, 0.9 ]  # Quantiles to be predicted if probabilistic (optional)
  hyperparameters:  # Model-specific hyperparameters. Check Darts and Model doc for available parameter (mandatory)
    lags: 5  # Params can be defined as fixed value or search space, e.g., {min: 1, max: 5, step: 1, type: int}
    ### Some parameter examples
    # learning_rate: {min: 1e-5, max: 1e0, type: float}
    # n_estimators: {min: 50, max: 500, step: 50, type: int}
    # objective: "RMSE"
    verbose: -1

Model_selection:
  n_trials: 5  # Number of parameter combinations from the defined search space to be tried (mandatory)
  timeout: 3600  # Timeout in seconds to interrupt hyperparameter study (mandatory)
  CV_splits: 1  # Number of train/val splits. If >1, time-series cross-validation applied (mandatory)
  train_size: 0.8  # Size of training set during model selection. Only relevant if no time-series CV used (mandatory)
  error_metric: "RMSE"  # 'RMSE','MAE','PINBALL','CUSTOM'. If 'CUSTOM', define metric in core.ModelEvaluator (mandatory)
  equal_weights_pred_steps: True  # Boolean if error of steps ahead should be weighted evenly for averaging (mandatory)
  error_weights: [ 1, 1, 1, 1 ]  # Weighting of step ahead errors if equal_weights_pred_steps = False (mandatory)
  pruning: False  #  Boolean to (de)activate optuna trial pruning. Not supported by all models (mandatory)
  early_stopping: False  # Boolean to (de)activate early stopping. Not supported by all models (mandatory)
  es_patience: 10  # Num of epochs without improvement before stopping training. Only if early_stopping=True (mandatory)
  retrain_dur_selection: False  # Retrain frequency (every n-th step) during selection. False to deactivate (optional)
  retrain_length_dur_selection:  # Length of moving retrain data window. If False, complete historical data. (optional)

Prediction:
  retrain_dur_prediction:  # Retrain freq (every n-th step) at test & new set prediction. False to deactivate (optional)
  retrain_length_dur_prediction:  # Length of moving retrain data window. If False, complete historical data. (optional)

Anomaly_detection:
  anomaly_window: 5  # Number of predictions to jointly evaluate for declaring anomalies (mandatory)
  threshold: 0.001  # Anomaly score above which observations are flagged. Highest score during test data=1 (mandatory)
  anomaly_types: True  # Boolean to (de)activate anomaly classification, e.g. positive or negative direction (mandatory)
  Anomaly_ground_truth:  # Description of anomaly ground truth data. If available, included in the plots (optional)
  ### Example of anomaly ground truth
  # [{"Type":"DoS","Victim":"PV_inv","Start":"2023-02-05 15:30:00","End":"2023-02-06 12:10:00","Reasoning":"..."},]

Results:
  save_test_set_metrics: True  # Boolean to decide whether to save performance metrics for test predictions (mandatory)
  save_new_set_metrics: True  # Boolean to decide whether to save performance metrics for new predictions (mandatory)
  save_parameter_importances: True  # Boolean whether to save parameter importances from model selection (mandatory)
  save_optimal_hyperparameters: True  # Boolean whether to save set of optimal params from model selection (mandatory)
  save_test_set_predictions: True  # Boolean whether to save predictions of the test set (mandatory)
  save_new_set_predictions: True  # Boolean whether to save predictions of the new set (mandatory)

Plotting:
  save_parameter_importances_plot: True  # Boolean whether to save plot of param importances from selection (mandatory)
  save_optimization_history_plot: True  # Boolean whether to save model selection progress plot (mandatory)
  save_test_set_predictions_plots: True  # Boolean whether to save plot of the test set predictions (mandatory)
  save_new_set_predictions_plots: True  # Boolean whether to save plot of the new set predictions (mandatory)
  save_shap_explainability_plots: False  # Boolean whether to save plot Shap explainability plots (mandatory)
  steps_to_plot: [1]  # List of steps ahead to be plotted. If empty or not provided, all steps plotted. (optional)

Computation:
  GPU: False  # Boolean to decide whether to train models on GPU. Not supported by all machines and models (mandatory)
  n_cores: 16  # Number of CPU cores to be used for parallel model training (mandatory)
  selection_on_multiple_machines: False  # Enables running selection on multiple machines via shared sql-db (mandatory)

```

## Directory Structure
To interact with FlexML, experiment configuration files and raw datasets must be placed in the appropriate directories. 
Experiment configurations should be stored in the `flexml/config/` directory. 
Raw datasets should be placed in the `flexml/datasets/raw/` folder.

Logs generated during an experiment run are saved in the `flexml/logs/` directory. 
The results of an experiment are stored in `flexml/results/`, with separate subfolders 
for model files, predictions, metrics, and plots.

The `flexml/modelhandlers/` directory contains the list of model handler classes, providing an 
overview of the models currently implemented in FlexML.

```bash
flexml/
├── config/                             # YAML Configuration files
│   ├── examples/                       # Configuration files of the provided example experiments
├── configvalidators/                   # Configuration classes for each component of the pipeline
├── core/                               # Core modules of the pipeline
├── datasets/                           # Datasets
│   ├── processed/                      # Processed datasets
│   ├── raw/                            # Raw historical and new datasets
├── logs/                               # Logs generated during an experiment run
├── modelhandlers/                      # Model handler child classes for each implemented model
├── notebooks/                          # Jupyter notebooks for experimentation
├── optuna_sql_databases/               # Optuna sql databases for distributed hyperparameter optimization
├── results/                            # Results of experiment runs
│   ├── metrics/                        # Performance metrics (rmse, mae, pinball, custom)
│   │   ├── new_set/                    # Metrics for predictions on new data
│   │   ├── test_set/                   # Metrics for predictions on test data
│   ├── models/                         # Models resulting from model selection
│   │   ├── optimal_hyperparameters/    # Optimal (and static) hyperparameters resulting from model selection
│   │   ├── parameter_importances/      # Hyperparameter importance determined during model selection
│   ├── plots/                          # Plots in svg format
│   │   ├── optimization_history/       # Plots describing the progress of the optimization process during model selection
│   │   ├── parameter_importances/      # Plots visualizing hyperparameter importance determined during model selection
│   │   ├── predictions/                # Plots of the deterministic/probabilistic predictions, including ground truth and anomaly flags
│   │   │   ├── new_set/                # Plots of new set predictions
│   │   │   ├── test_set/               # Plots of test set predictions
│   │   └── shap_explainer/             # Plots of Shap explainer for model interpretation
│   │       ├── test_set/               # Shap explainer plots on for test predictions
│   ├── predictions/                    # Predictions, including ground truth, anomaly scores and anomaly flags in csv format
│   │   ├── new_set/                    # New set predictions
│   │   ├── test_set/                   # Test set predictions
├── .gitignore                          # Files or directories to ignore in version control
├──                                     # Contains the license under which the project is distributed 
├── main.py                             # main.py to run the pipeline for a defined experiment configuration from the terminal
├── README.md                           # Project documentation
└── requirements.txt                    # Python dependencies    
```
## Examples
To demonstrate the functionality of FlexML, a concise example is provided. A historical and a new dataset is provided, consisting of
hourly photovoltaic (PV) production data, along with irradiation forecasts that serve 
as potential covariates. To apply FlexML to these datasets, the appropriate filenames must be specified, 
in this case, `hist_data_filename: 'historical_data_example.csv'`, as well as the column representing the 
feature `target_feature: 'PV_feed[kW]'`.

### Naive baseline
In the first example, a naive persistence forecasting model is employed. The associated config file is called
`config_example_naive.yaml`. The experiment name `Name: example_naive` is defined in the beginning of the config 
file to facilitate easy identification of results, which will include the defined name in the filenames.

To implement the persistence forecast, the model type `type: NaiveSeasonal` is chosen, as the hourly PV production 
exhibits a daily seasonal pattern. The forecasting horizon is set to three hours (`forecasting_horizon: 3`), meaning 
that the model predicts values for the next three hours at each step in a rolling fashion. 
The persistence model assumes that the future value will be identical to the value observed `K` steps in the past. 
A plausible assumption is `K=24`, but to validate this assumption, a model selection process is conducted by setting
`model_selection: True`.

The model selection process requires specifying the parameter range for K. This is done by defining a search space with 
`K: {min: 1, max: 26, step: 1, type: int}`, indicating that K will vary between 1 and 26 in steps of 1. Additionally, the
number of trials for parameter tuning is set to `n_trials: 50`.

Before initiating the experiment, the results to be saved are defined. This includes performance metrics for the test set
(`save_test_set_metrics: True`) and a plot of the test set predictions (`save_test_set_predictions_plots: True`). 
The specific forecasting steps for which plots should be generated are specified using `steps_to_plot: [1,3]`.

Before we start the experiment, we also define which results we want to save. For example, we are interested
in performance metrics on the test set `save_test_set_metrics: True` as well as a plot of the test set 
predictions `save_new_set_predictions_plots: True`. We further specify which steps ahead to create plots
for `steps_to_plot: [1,3]`. 

The experiment can then be executed with the following command:

```bash
python main.py -c config/examples/config_example_naive.yaml
```

After completion, the results of the model selection process can be found in the directory 
`flexml/results/models/optimal_hyperparameters`. As anticipated, the optimal value for 'K' is 24,
as indicated by the output:
`{'Fixed_params': {}, 'Optimal_hyperparameters': {'K': 24}}`

The performance metrics for the test set predictions are available in `flexml/results/metrics/test_set`.
These metrics are provided both per step-ahead and as averages. Given that the persistence model predicts 
future values based on the past values observed 24 hours earlier, the predictions and corresponding metrics
for all three forecasting steps are identical.

*Side note: If the prediction horizon is extended to >25 hours, the model will reference data from 48 hours prior,
as the 24-hour value would fall into the future and be unavailable.*
```
{
    "RMSE": {
        "Per_step_ahead": {
            "1_ahead": 0.7713251292005783,
            "2_ahead": 0.7713251292005783,
            "3_ahead": 0.7713251292005783
        },
        "Average": 0.7713251292005783
    },
    "MAE": {
        "Per_step_ahead": {
            "1_ahead": 0.4597159977824915,
            "2_ahead": 0.4597159977824915,
            "3_ahead": 0.4597159977824915
        },
        "Average": 0.45971599778249156
    }
}
```

The prediction plots for the 1-step and 3-step ahead forecasts are stored in `flexml/results/plots/predictions/test_set`. 
As expected, the predictions mirror the previous day's values for the forecasted day.

<img src="doc_figures/1737480268_NaiveSeasonal_1ahead_example_naive.svg" alt="Figure 1" width="500"/>
<img src="doc_figures/1737480268_NaiveSeasonal_3ahead_example_naive.svg" alt="Figure 1" width="500"/>

### AutoArima
In this example, the AutoARIMA model (`type: StatsForecastAutoARIMA`) is applied to the same dataset and target 
feature, using the configuration specified in the `config_example_autoarima.yaml`.

As AutoARIMA models automatically select hyperparameters based on heuristic criteria, such as AIC (Akaike Information 
Criterion) or BIC (Bayesian Information Criterion), model selection was disabled (`model_selection: False`). In this 
experiment, the forecasting horizon was set to 3 hours (`forecasting_horizon: 3`), and the only manually defined 
hyperparameter was the seasonal length, which was set to 24 hours (`season_length: 24`), based on prior knowledge.

The experiment is executed with the following command:

```bash
python main.py -c config/examples/config_example_autoarima.yaml
```

The performance metrics for the test set predictions indicate an increase in prediction errors with each step ahead,
which aligns with expectations. The model outperforms the naive persistence forecast for the 1-step ahead forecast. 
For the 2-step ahead forecast, performance is comparable, while for the 3-step ahead forecast, the seasonal persistence
method outperforms the AutoARIMA model.
```
{
    "RMSE": {
        "Per_step_ahead": {
            "1_ahead": 0.5670857057117449,
            "2_ahead": 0.7980903137248767,
            "3_ahead": 0.9752963688974305
        },
        "Average": 0.7801574627780173
    },
    "MAE": {
        "Per_step_ahead": {
            "1_ahead": 0.37717304485675285,
            "2_ahead": 0.5770787496346494,
            "3_ahead": 0.7367697777536625
        },
        "Average": 0.5636738574150216
    }
}
```
<img src="doc_figures/1737480392_StatsForecastAutoARIMA_1ahead_example_autoarima.svg" alt="Figure 1" width="500"/>
<img src="doc_figures/1737480392_StatsForecastAutoARIMA_3ahead_example_autoarima.svg" alt="Figure 1" width="500"/>

### Deterministic LightGBM
Next a LightGBM model is applied to the same problem by setting the model type to `type: LightGBMModel`.
The associated config file is `config_example_lightgbm_deterministic.yaml`.
The model is used out-of-the-box with its default parameters (`model_selection: False`).
For LightGBM models, the number of target feature lags used as input must be defined.
We set it to `lags: 48`, meaning that the model will process the past 48 target feature values 
to predict the next three steps ahead (`forecasting_horizon: 3`) in a rolling fashion. 

This time we will also calculate and plot Shap values `save_shap_explainability_plots: True` to interpret the
resulting model.

The experiment is executed with the following command:

```bash
python main.py -c config/examples/config_example_lightgbm_deterministic.yaml
```
As expected, the prediction error increases with each step ahead. However, the LightGBM model significantly
outperforms both the naive seasonal persistence and AutoARIMA forecasts across all steps ahead, even when 
using out-of-the-box hyperparameters, skipping model selection.

```
{
    "RMSE": {
        "Per_step_ahead": {
            "1_ahead": 0.45652379792073927,
            "2_ahead": 0.5779903106838254,
            "3_ahead": 0.6616567796428309
        },
        "Average": 0.5653902960824652
    },
    "MAE": {
        "Per_step_ahead": {
            "1_ahead": 0.27349712393527853,
            "2_ahead": 0.3442697644171845,
            "3_ahead": 0.38394342326080955
        },
        "Average": 0.3339034372044242
    }
}
```

<img src="doc_figures/1737480583_LightGBMModel_1ahead_example_lightgbm_deterministic.svg" alt="Figure 1" width="500"/>
<img src="doc_figures/1737480583_LightGBMModel_3ahead_example_lightgbm_deterministic.svg" alt="Figure 1" width="500"/>

The Shap beeswarm plots provide valuable insights into 1) the importance of individual features and 2)
how these features influence predictions.

Starting with the beeswarm plot for 1-step ahead predictions, the order of the features indicates their relative importance. 
The most influential feature is the most recent lag value (-1), while the lag value from 24 hours ago appears in third position. 
Interestingly, lag values from 1.5 days ago (-35 lag) also have a notable impact on the predictions.
The plot further reveals that high values of the -1 lag (indicated by red) strongly positively influence the predictions,
while low values (shown in blue) have a strong negative effect. This behavior aligns with expectations: if the last target 
feature value is high, the next value is typically high as well, and vice versa.

<img src="doc_figures/1737480583_LightGBMModel_1ahead_example_lightgbm_deterministic_shap.svg" alt="Figure 1" width="400"/>

In the beeswarm plot for 3-step ahead predictions, the -1 lag is less dominant, and the model relies on a broader set
of lag values to make predictions. Multiple lag values from various time frames now contribute substantially to the 
forecast. Notably, lag values from 0.5 or 1.5 days ago exhibit a strong but inverted influence. When these lag values
are low, they push the prediction toward higher values, and when they are high, they push the prediction toward lower
values. This behavior makes sense given the day/night cycle: if it was night 12 hours ago, there is likely to be sun
and a lot of PV production right now.

<img src="doc_figures/1737480583_LightGBMModel_3ahead_example_lightgbm_deterministic_shap.svg" alt="Figure 1" width="400"/>

### Probabilistic LightGBM
In this example, the LightGBM model is used for probabilistic predictions, enabling the quantification of uncertainty
in the forecasts. The associated config file is `config_example_lightgbm_probabilistic.yaml`. 
Key parameters are set as follows: `probabilistic: True` and `num_samples: 1000` to enable probabilistic forecasting with 
1000 samples. Additionally, `probability_model: 'quantile_reg'` is specified to perform quantile regression, with the
quantiles defined as `quantiles: [ 0.1, 0.5, 0.9 ]`.

The experiment is executed using the following command:

```bash
python main.py -c config/examples/config_example_lightgbm_probabilistic.yaml
```
The resulting metrics now automatically include the Pinball loss as a probabilistic error metric. 
Compared to the deterministic LightGBM model from the previous example, the RMSE has worsened, while the MAE has
improved. This change can be attributed to the switch in the objective function from RMSE to Pinball loss, which 
essentially is MAE on different quantiles.

```
{
    "RMSE": {
        "Per_step_ahead": {
            "1_ahead": 0.4697044144384659,
            "2_ahead": 0.5289532513458132,
            "3_ahead": 0.6025232671646755
        },
        "Average": 0.5337269776496515
    },
    "MAE": {
        "Per_step_ahead": {
            "1_ahead": 0.2598632397789529,
            "2_ahead": 0.30846944000026066,
            "3_ahead": 0.33340165674595273
        },
        "Average": 0.3005781121750555
    },
    "PINBALL": {
        "Per_step_ahead": {
            "1_ahead": 0.16897878538134944,
            "2_ahead": 0.18872568379500412,
            "3_ahead": 0.1953861442022308
        },
        "Average": 0.18436353779286144
    }
}
```

The test predictions plot now automatically include the prediction interval. It can be seen that the 
uncertainty about predictions increases with the forecasting horizon, as expected.

<img src="doc_figures/1737480946_LightGBMModel_1ahead_example_lightgbm_probabilistic.svg" alt="Figure 1" width="500"/>
<img src="doc_figures/1737480946_LightGBMModel_3ahead_example_lightgbm_probabilistic.svg" alt="Figure 1" width="500"/>

### Probabilistic LightGBM with Covariates
In this scenario, the irradiation forecast is included as a covariate. The config file is 
`config_example_lightgbm_probabilistic_covariates.yaml`. The only modification compared to the previous example 
is the addition of the future covariate definition: `future_covariates: ["Irradiance_forecast[W/m2]"]`.

The experiment is executed with the following command:

```bash
python main.py -c config/examples/config_example_lightgbm_probabilistic_covariates.yaml
```

The comparison of the calculated performance metrics with those from the previous model indicates that the 
inclusion of the irradiation forecast covariate significantly improves the predictions.

```
{
    "RMSE": {
        "Per_step_ahead": {
            "1_ahead": 0.30839712053289325,
            "2_ahead": 0.303766454917523,
            "3_ahead": 0.33427135554863185
        },
        "Average": 0.31547831033301604
    },
    "MAE": {
        "Per_step_ahead": {
            "1_ahead": 0.17444897721136032,
            "2_ahead": 0.168403479666708,
            "3_ahead": 0.19015555682655114
        },
        "Average": 0.17766933790153983
    },
    "PINBALL": {
        "Per_step_ahead": {
            "1_ahead": 0.11085635814378685,
            "2_ahead": 0.11127868001562631,
            "3_ahead": 0.13246891825945162
        },
        "Average": 0.11820131880628826
    }
}
```

<img src="doc_figures/1737481070_LightGBMModel_1ahead_example_lightgbm_probabilistic_covariates.svg" alt="Figure 1" width="500"/>
<img src="doc_figures/1737481070_LightGBMModel_3ahead_example_lightgbm_probabilistic_covariates.svg" alt="Figure 1" width="500"/>

The Shap plots reveal that the irradiation forecast is more informative than the target feature lag values. 
As to expect, for 3-step ahead predictions, the irradiation value for 3 hours ahead plays a dominant role, while 
for 1-step ahead predictions, the irradiation value from 1 hour ahead is most influential.

<img src="doc_figures/1737481070_LightGBMModel_1ahead_example_lightgbm_probabilistic_covariates_shap.svg" alt="Figure 1" width="400"/>
<img src="doc_figures/1737481070_LightGBMModel_3ahead_example_lightgbm_probabilistic_covariates_shap.svg" alt="Figure 1" width="400"/>

### Probabilistic LightGBM with Anomaly Detection
In this example, the probabilistic LightGBM model is applied for anomaly detection on a new dataset.
The associated config file is `config_example_anomaly_detection.yaml`.

*Side note: FlexML can only flag anomalies in new datasets, as the test set from historical data is used to fit the 
anomaly detector. Therefore, for anomaly detection, historical data (including both training and test data) should be 
free from anomalies. If only a historical dataset is available, it is advisable to split it into two sets, using the 
latter as the "new" dataset.*

To perform anomaly detection on a new dataset, the prediction on new data is enabled with the parameter 
`predict_new_data: True`, and the filename of the new dataset is specified as
`new_data_filename: "new_data_example.csv"`. This new dataset contains a recording of a PV inverter failure, leading
to a 2-hour interruption in the PV feed. Anomaly detection is then activated by setting `anomaly_detection: True`. 

The anomaly detector is configured with `anomaly_window: 1`, meaning that anomaly scores are 
calculated based only on the most recent prediction. By setting `threshold: 3`, anomalies are flagged if the 
anomaly score for a prediction in the new dataset exceeds three times the highest anomaly score from the test data. 
 Additionally, anomaly classification is enabled with `anomaly_types: True`, which provides further details about
the anomaly, such as whether it is an abnormally high value or zero. This information can be valuable for automated
root cause analysis.

If ground truth labels for anomalies are available, they can be provided in the config file in the following format:
`Anomaly_ground_truth: [{"Type":"Technical failure","Victim":"PV inverter", "Start":"2022-09-19 11:00:00", "End":"2022-09-19 12:00:00", "Reasoning":""},]`
These labels are not used by the anomaly detector but are included for evaluation purposes and will appear in the 
new data prediction plots.

To visually evaluate the results of the anomaly detection, a new test set prediction plot is generated by setting
`save_new_set_predictions_plots: True`.

The experiment is executed using the following command:
```bash
python main.py -c config/examples/config_example_anomaly_detection.yaml
```

The new set prediction plot shows that the PV inverter failure results in a high anomaly score, which is 
flagged as an anomaly. The anomaly classification feature also reveals that the PV feed during the anomaly
period is abnormally low and even zero, indicating a complete interruption of the PV feed for two hours.

<img src="doc_figures/1737481351_LightGBMModel_1ahead_example_anomaly_detection.svg" alt="Figure 1" width="500"/>


## Contributing

To contribute to FlexML, please follow the steps outlined below:

1. Fork the repository.
2. Create a new branch: `git checkout -b feature-name`.
3. Implement the desired changes.
4. Push the branch: `git push origin feature-name`.
5. Submit a pull request for review.

## License
This project is licensed under the [MIT License](LICENSE.txt).